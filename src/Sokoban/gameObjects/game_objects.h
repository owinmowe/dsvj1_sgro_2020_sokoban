#ifndef GAME_OBJECTS
#define GAME_OBJECTS

namespace game_objects
{
	enum class DIRECTION { LEFT, UP, RIGHT, DOWN };
	enum class SKIN_TYPE {SKELETON, ZOMBIE, MUMMY};
}

#endif // !GAME_OBJECTS

