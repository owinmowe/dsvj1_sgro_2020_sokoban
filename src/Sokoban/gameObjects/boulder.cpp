#include "boulder.h"
#include "Draw/textures.h"
#include "ScreenConfig/screen_config.h"

namespace game_objects
{

	const int BOULDER_TYPES = 3;
	const float GLOW_SPEED = 5.f;
	const float MOVE_SPEED = .075f;

	Boulder::Boulder(Vector2 tilesPosition)
	{
		texture = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::BOULDERS);
		currentSourceRecInactive = { 0, 0, static_cast<float>(texture->width / BOULDER_TYPES), static_cast<float>(texture->height / 2) };
		currentSourceRecActive = { 0, static_cast<float>(texture->height / 2), static_cast<float>(texture->width / BOULDER_TYPES), static_cast<float>(texture->height / 2) };
		currentTilesPosition = tilesPosition;
		target = { 0,0 };
		moving = false;
		active = false;
		glowing = false;
		moveDir = DIRECTION::LEFT;
	}
	Boulder::~Boulder()
	{

	}
	void Boulder::draw()
	{
		if (texture != nullptr)
		{
			if(active)
			{
				DrawTexturePro(*texture, currentSourceRecInactive, currentWorldRec, { 0,0 }, 0, WHITE);
				DrawTexturePro(*texture, currentSourceRecActive, currentWorldRec, { 0,0 }, 0, activeColor);
			}
			else
			{
				DrawTexturePro(*texture, currentSourceRecInactive, currentWorldRec, { 0,0 }, 0, WHITE);
			}
		}
	}
	void Boulder::update()
	{
		if(moving)
		{
			switch (moveDir)
			{
			case game_objects::DIRECTION::LEFT:
				currentWorldRec.x -= MOVE_SPEED * GetFrameTime() * screenConfig::currentScreenWidth;
				if(currentWorldRec.x < target.x)
				{
					currentWorldRec.x = target.x;
					currentTilesPosition.x--;
					moving = false;
				}
				break;
			case game_objects::DIRECTION::UP:
				currentWorldRec.y -= MOVE_SPEED * GetFrameTime() * screenConfig::currentScreenWidth;
				if (currentWorldRec.y < target.y)
				{
					currentWorldRec.y = target.y;
					currentTilesPosition.y--;
					moving = false;
				}
				break;
			case game_objects::DIRECTION::RIGHT:
				currentWorldRec.x += MOVE_SPEED * GetFrameTime() * screenConfig::currentScreenWidth;
				if (currentWorldRec.x > target.x)
				{
					currentWorldRec.x = target.x;
					currentTilesPosition.x++;
					moving = false;
				}
				break;
			case game_objects::DIRECTION::DOWN:
				currentWorldRec.y += MOVE_SPEED * GetFrameTime() * screenConfig::currentScreenWidth;
				if (currentWorldRec.y > target.y)
				{
					currentWorldRec.y = target.y;
					currentTilesPosition.y++;
					moving = false;
				}
				break;
			default:
				break;
			}
		}
		if(active)
		{
			if(glowing)
			{
				if (activeColor.a + GLOW_SPEED < 255)
				{
					activeColor.a += static_cast<unsigned char>(GLOW_SPEED);
				}
				else
				{
					activeColor.a = static_cast<unsigned char>(255);
					glowing = false;
				}
			}
			else
			{
				if (activeColor.a - GLOW_SPEED > 0)
				{
					activeColor.a -= static_cast<unsigned char>(GLOW_SPEED);
				}
				else
				{
					activeColor.a = 0;
					glowing = true;
				}
			}
		}
	}
	void Boulder::move(DIRECTION dir)
	{
		moveDir = dir;
		switch (dir)
		{
		case game_objects::DIRECTION::LEFT:
			target = { currentWorldRec.x - currentTileSize.x, currentWorldRec.y };
			break;
		case game_objects::DIRECTION::UP:
			target = { currentWorldRec.x, currentWorldRec.y - currentTileSize.y };
			break;
		case game_objects::DIRECTION::RIGHT:
			target = { currentWorldRec.x + currentTileSize.x, currentWorldRec.y };
			break;
		case game_objects::DIRECTION::DOWN:
			target = { currentWorldRec.x, currentWorldRec.y + currentTileSize.y };
			break;
		default:
			break;
		}
		moving = true;
	}
	void Boulder::setPositionFromZero(Vector2 tileSize, Vector2 newPos)
	{
		currentTileSize = tileSize;
		currentWorldRec = { newPos.x, newPos.y, currentTileSize.x, currentTileSize.y };
	}
	Vector2 Boulder::getTilePosition()
	{
		return currentTilesPosition;
	}
	void Boulder::setActive(bool activeState)
	{
		active = activeState;
	}
	bool Boulder::isActive()
	{
		return active;
	}
	void Boulder::moveHalfTile()
	{
		currentWorldRec.x += currentWorldRec.width / 2;
	}
	void Boulder::setSkin(SKIN_TYPE type)
	{
		switch (type)
		{
		case game_objects::SKIN_TYPE::SKELETON:
			currentSourceRecActive.x = 0;
			currentSourceRecInactive.x = 0;
			break;
		case game_objects::SKIN_TYPE::ZOMBIE:
			currentSourceRecActive.x = currentSourceRecActive.width;
			currentSourceRecInactive.x = currentSourceRecActive.width;
			break;
		case game_objects::SKIN_TYPE::MUMMY:
			currentSourceRecActive.x = currentSourceRecActive.width * 2;
			currentSourceRecInactive.x = currentSourceRecActive.width * 2;
			break;
		default:
			break;
		}
	}
}