#ifndef BOULDER_H
#define BOULDER_H

#include "raylib.h"
#include "game_objects.h"

namespace game_objects
{
	class Boulder
	{
	public:
		Boulder(Vector2 tilesPosition);
		~Boulder();
		void draw();
		void update();
		void move(DIRECTION dir);
		void setPositionFromZero(Vector2 tileSize, Vector2 newPos);
		void setActive(bool activeState);
		void setSkin(SKIN_TYPE type);
		bool isActive();
		Vector2 getTilePosition();
		void moveHalfTile();
	private:
		bool moving = false;
		bool active = false;
		bool glowing = false;
		Texture2D* texture = nullptr;
		Rectangle currentSourceRecInactive = { 0, 0, 0, 0 };
		Rectangle currentSourceRecActive = { 0, 0, 0, 0 };
		Rectangle currentWorldRec = { 0, 0, 0, 0 };
		Vector2 currentTileSize = { 0,0 };
		Vector2 currentTilesPosition = { 0,0 };
		Color activeColor = { 255, 255, 255, 0 };
		Vector2 target = { 0,0 };
		DIRECTION moveDir = DIRECTION::LEFT;
	};
}

#endif // !BOULDER_H

