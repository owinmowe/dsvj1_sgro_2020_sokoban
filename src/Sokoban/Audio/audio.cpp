#include "audio.h"
#include "raylib.h"
#include <vector>
#include <string>
#include "EventSystem/event_system.h"
#include "Scenes/scene_manager.h"

namespace audio
{

    float musicVolume = 0.3f;
    float soundVolume = 0.1f;
    const int MAX_SIZE_FILEPATH = 100;

    struct SoundFile
    {
        Sound sound;
        char FILE_PATH[MAX_SIZE_FILEPATH];
    };

    struct MusicFile
    {
        Music music;
        char FILE_PATH[MAX_SIZE_FILEPATH];
    };

    std::vector<MusicFile> musicFiles;
    enum class MUSIC_ID{MENUMUSIC_ID, GAMEMUSIC_ID, MENU_SELECT_ID, WON_LEVEL_ID, MOVE_BOULDER_ID, SWITCH_ACTIVATED_ID};

    std::vector<SoundFile> soundFiles;
    enum class SOUND_ID{ MENU_MOVE_ID, MOVE_COMMON_ID, MOVE_BLOCKED_ID, LOST_LEVEL_ID};

    void musicAsSoundEvent(MUSIC_ID music_ID, event_system::EVENT_ID event_ID)
    {
        if (event_system::externalEventSystem->checkEvent(event_ID))
        {
            musicFiles[static_cast<int>(music_ID)].music.loopCount = 1;
            StopMusicStream(musicFiles[static_cast<int>(music_ID)].music);
            PlayMusicStream(musicFiles[static_cast<int>(music_ID)].music);
        }
        if (IsMusicPlaying(musicFiles[static_cast<int>(music_ID)].music))
        {
            UpdateMusicStream(musicFiles[static_cast<int>(music_ID)].music);
        }
    }

    void setMusicVector()
    {
        musicFiles.push_back({ Music(), "res/assets/audio/music/MenuMusic.mp3" });
        musicFiles.push_back({ Music(), "res/assets/audio/music/GameMusic.mp3" });
        musicFiles.push_back({ Music(), "res/assets/audio/music/MenuSelect.mp3" });
        musicFiles.push_back({ Music(), "res/assets/audio/music/LevelWon.mp3" });
        musicFiles.push_back({ Music(), "res/assets/audio/sounds/BoulderMove.mp3" });
        musicFiles.push_back({ Music(), "res/assets/audio/sounds/SwitchActivated.mp3" });
    }

    void setSoundVector()
    {
        soundFiles.push_back({ Sound(), "res/assets/audio/sounds/MenuMove.wav" });
        soundFiles.push_back({ Sound(), "res/assets/audio/sounds/MoveCommon.wav" });
        soundFiles.push_back({ Sound(), "res/assets/audio/sounds/MoveBlocked.wav" });
        soundFiles.push_back({ Sound(), "res/assets/audio/sounds/ResetLevel.wav" });
    }

    void setAllSoundVolume()
    {
        for (SoundFile& file : soundFiles)
        {
            SetSoundVolume(file.sound, soundVolume);
        }
    }

    void setAllMusicVolume()
    {
        for (MusicFile& file : musicFiles)
        {
            SetMusicVolume(file.music, musicVolume);
        }
    }

    void loadSoundsInMemory()
    {
        setSoundVector();
        for(SoundFile& file : soundFiles)
        {
            file.sound = LoadSound(file.FILE_PATH);
        }
    }

    void unloadSoundsFromMemory()
    {
        for(SoundFile& file : soundFiles)
        {
            UnloadSound(file.sound);
        }
        soundFiles.clear();
    }

    void loadMusicInMemory()
    {
        setMusicVector();
        for(MusicFile& file : musicFiles)
        {
            file.music = LoadMusicStream(file.FILE_PATH);
        }
    }

    void unloadMusicFromMemory()
    {
        for(MusicFile& file : musicFiles)
        {
            UnloadMusicStream(file.music);
        }
        musicFiles.clear();
    }

    void menuAudioUpdate()
    {
        if (sceneConfig::musicOn)
        {
            UpdateMusicStream(musicFiles[static_cast<int>(MUSIC_ID::MENUMUSIC_ID)].music);
        }
        if (sceneConfig::sfxOn)
        {
            if (event_system::externalEventSystem->checkEvent(event_system::EVENT_ID::MOVE_MENU)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::MENU_MOVE_ID)].sound); }
            musicAsSoundEvent(MUSIC_ID::MENU_SELECT_ID, event_system::EVENT_ID::SELECT_MENU);
        }
    }
    void menuAudioStart()
    {
        StopMusicStream(musicFiles[static_cast<int>(MUSIC_ID::GAMEMUSIC_ID)].music);
        PlayMusicStream(musicFiles[static_cast<int>(MUSIC_ID::MENUMUSIC_ID)].music);
    }

    void gameAudioUpdate()
    {
        using namespace event_system;
        if (sceneConfig::musicOn)
        {
            UpdateMusicStream(musicFiles[static_cast<int>(MUSIC_ID::GAMEMUSIC_ID)].music);
        }
        if (sceneConfig::sfxOn)
        {
            if (externalEventSystem->checkEvent(EVENT_ID::MOVE_MENU)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::MENU_MOVE_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::RESET_LEVEL)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::LOST_LEVEL_ID)].sound); }
            musicAsSoundEvent(MUSIC_ID::WON_LEVEL_ID, EVENT_ID::WON_LEVEL);
            musicAsSoundEvent(MUSIC_ID::MENU_SELECT_ID, EVENT_ID::SELECT_MENU);
            musicAsSoundEvent(MUSIC_ID::SWITCH_ACTIVATED_ID, EVENT_ID::ACTIVATE_SWITCH);
            musicAsSoundEvent(MUSIC_ID::MOVE_BOULDER_ID, EVENT_ID::MOVE_BOULDER);
        }
    }
    void gameAudioStart()
    {
        StopMusicStream(musicFiles[static_cast<int>(MUSIC_ID::MENUMUSIC_ID)].music);
        PlayMusicStream(musicFiles[static_cast<int>(MUSIC_ID::GAMEMUSIC_ID)].music);
    }
}