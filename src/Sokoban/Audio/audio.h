#ifndef AUDIO_H
#define AUDIO_H

namespace audio
{

	extern float musicVolume;
	extern float soundVolume;

	void menuAudioUpdate();
	void menuAudioStart();
	void gameAudioUpdate();
	void gameAudioStart();
	void loadSoundsInMemory();
	void setAllSoundVolume();
	void unloadSoundsFromMemory();
	void loadMusicInMemory();
	void unloadMusicFromMemory();
	void setAllMusicVolume();

}

#endif AUDIO_H
