#include "Sokoban.h"
#include "raylib.h"
#include "Scenes/scene_manager.h"
#include "Input/Input.h"
#include "Update/update.h"
#include "Draw/draw.h"
#include "Draw/textures.h"
#include "Audio/audio.h"
#include "ScreenConfig/screen_config.h"
#include "EventSystem/event_system.h"

Sokoban::Sokoban()
{
    InitWindow(screenConfig::currentScreenWidth, screenConfig::currentScreenHeight, "Evil Army");
    InitAudioDevice();
    audio::loadMusicInMemory();
    audio::loadSoundsInMemory();
    audio::setAllSoundVolume();
    audio::setAllMusicVolume();
    textures::LoadUiTextures();
    event_system::externalEventSystem = new event_system::EventSystem;
    SetTargetFPS(screenConfig::FRAMES_PER_SECOND);
    //ToggleFullscreen();
    SetExitKey(0);
    HideCursor();
    sceneConfig::ChangeSceneTo(sceneConfig::Scene::MAIN_MENU);
}
Sokoban::~Sokoban()
{
    delete sceneConfig::currentScene_Ptr;
    StopSoundMulti();
    audio::unloadMusicFromMemory();
    audio::unloadSoundsFromMemory();
    textures::UnloadUiTextures();
    if (IsWindowFullscreen()) { ToggleFullscreen(); } // Go to windows mode before closing to avoid crashing opengl. 
    CloseWindow();
}
void Sokoban::play()
{
    while (!WindowShouldClose() && sceneConfig::playing)
    {
        input::updateInputGame();
        updateGame();
        drawGame();
        event_system::externalEventSystem->ResetEvents();
    }
}