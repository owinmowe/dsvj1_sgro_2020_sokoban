#include "loader.h"
#include "raylib.h"
#include <string>

namespace loader
{
	const char SAVE_TILES_FILE_PATH[] = "res/assets/levels/levels.dat";
	const char LEVEL_START_CHARACTER = '~';

	void loadLevelFromFile(int level, FloorTiles* tiles[GRID_SIZE_X][GRID_SIZE_Y], player::Player* currentPlayer, std::vector<Boulder*>& boulders)
	{
		char* charTextFile = LoadFileText(SAVE_TILES_FILE_PATH);
		std::string stringTextFile(charTextFile);

		int startingPos = 0;
		int auxLevel = 0;
		while (auxLevel != level)
		{
			if(stringTextFile[startingPos] == LEVEL_START_CHARACTER)
			{
				auxLevel++;
			}
			startingPos++;
		}
		startingPos++;
		for (int i = 0; i < static_cast<int>(boulders.size()); i++)
		{
			delete boulders[i];
		}
		boulders.clear();

		for (int j = 0; j < GRID_SIZE_Y; j++)
		{
			for (int k = 0; k < GRID_SIZE_X; k++)
			{
				switch (stringTextFile[startingPos + k + j * (GRID_SIZE_X + 1)])
				{
				case '1':
					tiles[k][j]->activate();
					break;
				case 'P':
				case 'p':
					tiles[k][j]->activate();
					currentPlayer->setPositionFromZero({ static_cast<float>(k), static_cast<float>(j) });
					break;
				case 'B':
				case 'b':
					tiles[k][j]->activate();
					boulders.push_back(new Boulder({ static_cast<float>(k), static_cast<float>(j) }));
					break;
				case 'S':
				case 's':
					tiles[k][j]->activate();
					tiles[k][j]->makeTrigger();
					break;
				case 'W':
				case 'w':
					tiles[k][j]->makeWall();
					break;
				default:
					break;
				}
			}
		}
	}
}