#ifndef LOADER_H
#define LOADER_H

#include <vector>
#include "GameObjects/floor_tiles.h"
#include "GameObjects/player.h"
#include "gameObjects/boulder.h"

namespace loader
{
	using namespace game_objects;
	void loadLevelFromFile(int level, FloorTiles* tiles[GRID_SIZE_X][GRID_SIZE_Y], player::Player* currentPlayer, std::vector<Boulder*>& boulders);
}

#endif // !LOADER_H

