#ifndef EVENT_SYSTEM
#define EVENT_SYSTEM

namespace event_system
{

	enum class EVENT_ID { MOVE_MENU, SELECT_MENU, MOVE_COMMON, MOVE_BLOCKED,  ACTIVATE_SWITCH, MOVE_BOULDER, WON_LEVEL, RESET_LEVEL };

	class EventSystem
	{
	private:
		bool moveMenuBool;
		bool selectMenuBool;
		bool activateSwitchBool;
		bool moveCommonBool;
		bool moveBlockedBool;
		bool moveBoulderBool;
		bool wonLevelBool;
		bool resetLevelBool;
	public:
		EventSystem();
		~EventSystem();
		void setEvent(EVENT_ID eventNumber);
		bool checkEvent(EVENT_ID eventNumber);
		void ResetEvents();
	};

	extern EventSystem* externalEventSystem;

}

#endif