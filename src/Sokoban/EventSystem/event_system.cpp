#include "event_system.h"
#include "Scenes/scene_manager.h"

namespace event_system
{

	EventSystem* externalEventSystem;

	EventSystem::EventSystem()
	{
		moveMenuBool = false;
		selectMenuBool = false;
		activateSwitchBool = false;
		moveBoulderBool = false;
		moveCommonBool = false;
		moveBlockedBool = false;
		wonLevelBool = false;
		resetLevelBool = false;
	}
	EventSystem::~EventSystem()
	{

	}
	void EventSystem::setEvent(EVENT_ID eventNumber)
	{
		switch (eventNumber)
		{
		case event_system::EVENT_ID::MOVE_MENU:
			moveMenuBool = true;
			break;
		case event_system::EVENT_ID::SELECT_MENU:
			selectMenuBool = true;
			break;
		case event_system::EVENT_ID::MOVE_COMMON:
			moveCommonBool = true;
			break;
		case event_system::EVENT_ID::MOVE_BLOCKED:
			moveBlockedBool = true;
			break;
		case event_system::EVENT_ID::ACTIVATE_SWITCH:
			activateSwitchBool = true;
			break;
		case event_system::EVENT_ID::MOVE_BOULDER:
			moveBoulderBool = true;
			break;
		case event_system::EVENT_ID::WON_LEVEL:
			wonLevelBool = true;
			break;
		case event_system::EVENT_ID::RESET_LEVEL:
			resetLevelBool = true;
			break;
		default:
			break;
		}
	}
	bool EventSystem::checkEvent(EVENT_ID eventNumber)
	{
		switch (eventNumber)
		{
		case event_system::EVENT_ID::MOVE_MENU:
			return moveMenuBool;
			break;
		case event_system::EVENT_ID::SELECT_MENU:
			return selectMenuBool;
			break;
		case event_system::EVENT_ID::MOVE_COMMON:
			return moveCommonBool;
			break;
		case event_system::EVENT_ID::MOVE_BLOCKED:
			return moveBlockedBool;
			break;
		case event_system::EVENT_ID::ACTIVATE_SWITCH:
			return activateSwitchBool;
			break;
		case event_system::EVENT_ID::MOVE_BOULDER:
			return moveBoulderBool;
			break;
		case event_system::EVENT_ID::WON_LEVEL:
			return wonLevelBool;
			break;
		case event_system::EVENT_ID::RESET_LEVEL:
			return resetLevelBool;
			break;
		default:
			return false;
			break;
		}
	}
	void EventSystem::ResetEvents()
	{
		moveMenuBool = false;
		selectMenuBool = false;
		activateSwitchBool = false;
		moveCommonBool = false;
		moveBlockedBool = false;
		moveBoulderBool = false;
		wonLevelBool = false;
		resetLevelBool = false;
		sceneConfig::sceneChange = false;
	}
}