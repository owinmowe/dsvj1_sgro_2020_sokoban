#ifndef LEVEL_SUMMONS_H
#define LEVEL_SUMMONS_H
#include "raylib.h"
#include <string>

namespace game_objects
{
	namespace summons
	{
		enum class SUMMON_TYPE { SKELETON = 1, SKELETON_ARCHER, DEATH_KNIGHT, GHOUL, GHOUL_ARCHER, LICH, MUMMY, MUMMY_PHARAOH, ANUBIS};

		class LevelSummon
		{
		public:
			LevelSummon(SUMMON_TYPE summon);
			~LevelSummon();
			void draw();
			void update();
		private:
			std::string text;
			bool active = false;
			Color currentColor = {0, 0, 0, 255};
			Texture2D* overlay = nullptr;
			Texture2D* currentSummonAtlas = nullptr;
			Rectangle currentWorldOverlayRectangle = { 0,0,0,0 };
			Rectangle currentSourceSummonRectangle = { 0,0,0,0 };
			Rectangle currentWorldSummonRectangle = { 0,0,0,0 };
		};
	}
}

#endif