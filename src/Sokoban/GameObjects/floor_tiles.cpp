#include "floor_tiles.h"
#include "Draw/textures.h"

namespace game_objects
{

	const float GLOW_SPEED = 5.f;

	FloorTiles::FloorTiles(Rectangle newRec)
	{
		active = false;
		wall = false;
		trigger = false;
		texture = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::TILES);
		currentSourceRec = { 0, 0, static_cast<float>(texture->width / TILES_SKINS_AMMOUNT), static_cast<float>(texture->height) / TILES_TYPES_AMMOUNT };
		currentWorldRec = newRec;
	}
	FloorTiles::~FloorTiles()
	{

	}
	void FloorTiles::activate()
	{
		active = true;
	}
	void FloorTiles::makeTrigger()
	{
		trigger = true;
		glowing = true;
	}
	void FloorTiles::makeWall()
	{
		wall = true;
		currentSourceRec.y += currentSourceRec.height * 2;
	}
	bool FloorTiles::isActive()
	{
		return active;
	}
	void FloorTiles::draw()
	{
		if(texture != nullptr)
		{

			DrawTexturePro(*texture, currentSourceRec, currentWorldRec, { 0,0 }, 0, WHITE);
			if(trigger)
			{
				if (glowing)
				{
					if (activeColor.a + GLOW_SPEED < 255)
					{
						activeColor.a += static_cast<unsigned char>(GLOW_SPEED);
					}
					else
					{
						activeColor.a = static_cast<unsigned char>(255);
						glowing = false;
					}
				}
				else
				{
					if (activeColor.a - GLOW_SPEED > 0)
					{
						activeColor.a -= static_cast<unsigned char>(GLOW_SPEED);
					}
					else
					{
						activeColor.a = 0;
						glowing = true;
					}
				}
				DrawTexturePro(*texture, { currentSourceRec.x, currentSourceRec.height, currentSourceRec.width, currentSourceRec.height }, currentWorldRec, { 0,0 }, 0, activeColor);
			}
		}
	}
	bool FloorTiles::isTrigger()
	{
		return trigger;
	}
	bool FloorTiles::isWall()
	{
		return wall;
	}
	void FloorTiles::moveHalfTile()
	{
		currentWorldRec.x += currentWorldRec.width / 2;
	}
	void FloorTiles::setSkin(SKIN_TYPE type)
	{
		switch (type)
		{
		case game_objects::SKIN_TYPE::SKELETON:
			currentSourceRec.x = 0;
			break;
		case game_objects::SKIN_TYPE::ZOMBIE:
			currentSourceRec.x = currentSourceRec.width;
			break;
		case game_objects::SKIN_TYPE::MUMMY:
			currentSourceRec.x = currentSourceRec.width * 2;
			break;
		default:
			break;
		}
	}
}