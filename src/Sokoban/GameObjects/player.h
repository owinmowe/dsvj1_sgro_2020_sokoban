#ifndef PLAYER_H
#define PLAYER_H

#include "raylib.h"
#include "game_objects.h"

namespace game_objects
{

	namespace player
	{

		enum class ACTION { IDLE, WALKING, PUSHING };

		class Player
		{
		public:
			Player(Vector2 tileSize, Vector2 playerCurrentTilePos, Vector2 newPos);
			~Player();
			void draw();
			void update();
			void move();
			void setDirection(DIRECTION dir);
			void setAction(ACTION action);
			ACTION getAction();
			Vector2 getCurrentTilePosition();
			void setPositionFromZero(Vector2 newTilePos);
			bool isMoving();
			void moveHalfTile();
		private:
			Vector2 currentTilesPosition = { 0,0 };
			Vector2 currentTileSize = { 0,0 };
			Vector2 target = { 0,0 };
			bool moving = false;
			Texture2D* textureAtlas = nullptr;
			Rectangle currentSourceRec = { 0, 0, 0, 0 };
			Rectangle currentWorldRect = { 0, 0, 0, 0 };
			DIRECTION currentDirection = DIRECTION::DOWN;
			ACTION currentAction = ACTION::IDLE;
		};
	}
}
#endif // !PLAYER_H

