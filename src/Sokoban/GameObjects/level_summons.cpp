#include "level_summons.h"
#include "ScreenConfig/screen_config.h"
#include "Draw/textures.h"
#include "Ui/ui.h"

namespace game_objects
{
    namespace summons
    {
        const Rectangle OVERLAY_REC = { .35f, .65f, .3f, .35f };
        const Rectangle SUMMON_REC = { .425f, .75f, .15f, .2f };
        const Vector2 TEXT_POSITION = { .5f, .95f };
        const float TEXT_SIZE = .02f;
        const int ANIMATIONS_AMMOUNT = 18; //Ammount of move animations in the atlas 
        const float TRANSITION_SPEED = 5.f;

        LevelSummon::LevelSummon(SUMMON_TYPE summon)
        {
            active = false;
            currentColor = { 0, 0, 0, 255 };
            overlay = nullptr;
            currentSummonAtlas = nullptr;
            switch (summon)
            {
            case SUMMON_TYPE::SKELETON:
                currentSummonAtlas = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::SKELETON);
                text = "Skeleton joins your army!";
                break;
            case SUMMON_TYPE::GHOUL:
                currentSummonAtlas = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::GHOUL);
                text = "Ghoul joins your army!";
                break;
            case SUMMON_TYPE::MUMMY:
                currentSummonAtlas = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::MUMMY);
                text = "Mummy joins your army!";
                break;
            case SUMMON_TYPE::SKELETON_ARCHER:
                currentSummonAtlas = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::SKELETON_ARCHER);
                text = "Skeleton archer joins your army!";
                break;
            case SUMMON_TYPE::GHOUL_ARCHER:
                currentSummonAtlas = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::GHOUL_ARCHER);
                text = "Ghoul archer joins your army!";
                break;
            case SUMMON_TYPE::ANUBIS:
                currentSummonAtlas = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::ANUBIS);
                text = "Anubis joins your army!";
                break;
            case SUMMON_TYPE::DEATH_KNIGHT:
                currentSummonAtlas = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::DEATH_KNIGHT);
                text = "Death Knight joins your army!";
                break;
            case SUMMON_TYPE::LICH:
                currentSummonAtlas = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::LICH);
                text = "Lich joins your army!";
                break;
            case SUMMON_TYPE::MUMMY_PHARAOH:
                currentSummonAtlas = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::MUMMY_PHARAOH);
                text = "Mummy Pharaoh joins your army!";
                break;
            default:
                currentSummonAtlas = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::SKELETON);
                text = "The skeleton joins your army!";
                break;
            }
            overlay = textures::getUiTexture(textures::UI_TEXTURES_ID::OVERLAY);
            currentWorldOverlayRectangle = screenConfig::screenModifier(OVERLAY_REC);
            currentSourceSummonRectangle = { 0, 0, static_cast<float>(currentSummonAtlas->width / ANIMATIONS_AMMOUNT), static_cast<float>(currentSummonAtlas->height) };
            currentWorldSummonRectangle = screenConfig::screenModifier(SUMMON_REC);
        }
        LevelSummon::~LevelSummon()
        {

        }
        void LevelSummon::draw()
        {
            DrawTexturePro(*overlay, { 0,0, static_cast<float>(overlay->width), static_cast<float>(overlay->height) }, currentWorldOverlayRectangle, { 0,0 }, 0, WHITE);
            DrawTexturePro(*currentSummonAtlas, currentSourceSummonRectangle, currentWorldSummonRectangle, { 0,0 }, 0, currentColor);
            if(active)
            {
                Vector2 aux = screenConfig::screenModifier(TEXT_POSITION);
                ui::drawTextWithSecondFont(text.c_str(), aux.x, aux.y, screenConfig::textScreenModifier(TEXT_SIZE), DARKGRAY);
            }
        }
        void LevelSummon::update()
        {
            if (currentColor.r + TRANSITION_SPEED < 255)
            {
                currentColor.r += static_cast<unsigned char>(TRANSITION_SPEED);
                currentColor.g += static_cast<unsigned char>(TRANSITION_SPEED);
                currentColor.b += static_cast<unsigned char>(TRANSITION_SPEED);
            }
            else if(screenConfig::currentFrame % 3 == 0)
            {
                currentColor.r = static_cast<unsigned char>(255);
                currentColor.g = static_cast<unsigned char>(255);
                currentColor.b = static_cast<unsigned char>(255);
                active = true;
                currentSourceSummonRectangle.x += static_cast<float>(currentSummonAtlas->width) / static_cast<float>(ANIMATIONS_AMMOUNT);
                if (currentSourceSummonRectangle.x > currentSummonAtlas->width)
                {
                    currentSourceSummonRectangle.x = 0;
                }
            }
        }
    }
}