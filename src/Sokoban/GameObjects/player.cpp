#include "player.h"
#include "Draw/textures.h"
#include "ScreenConfig/screen_config.h"

namespace game_objects
{
	namespace player
	{

		const int ANIMATIONS_FRAMES_AMMOUNT = 18;
		const int ACTION_TYPES = 3;
		const int TOTAL_ANIMATIONS_TYPES = 12;
		const float MOVE_SPEED = .075f;

		Player::Player(Vector2 tileSize, Vector2 tilesPosition, Vector2 newPos)
		{
			textureAtlas = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::PLAYER);
			currentSourceRec = { 0, 0, static_cast<float>(textureAtlas->width / ANIMATIONS_FRAMES_AMMOUNT), static_cast<float>(textureAtlas->height / TOTAL_ANIMATIONS_TYPES) };
			currentTileSize = tileSize;
			currentTilesPosition = tilesPosition;
			currentWorldRect = { newPos.x, newPos.y, currentTileSize.x, currentTileSize.y};
			currentDirection = DIRECTION::DOWN;
			currentAction = ACTION::IDLE;
			target = { 0,0 };
			moving = false;
		}
		Player::~Player()
		{

		}
		void Player::draw()
		{
			if(textureAtlas != nullptr)
			{
				DrawTexturePro(*textureAtlas, currentSourceRec, currentWorldRect, { 0,0 }, 0, WHITE);
			}
		}
		void Player::update()
		{
			if(screenConfig::currentFrame % 3 == 0)
			{
				currentSourceRec.x += static_cast<float>(textureAtlas->width) / ANIMATIONS_FRAMES_AMMOUNT;
				if (currentSourceRec.x > static_cast<float>(textureAtlas->width)) { currentSourceRec.x = 0; }
			}
			if(moving)
			{
				switch (currentDirection)
				{
				case game_objects::DIRECTION::LEFT:
					currentWorldRect.x -= MOVE_SPEED * GetFrameTime() * screenConfig::currentScreenWidth;
					if(currentWorldRect.x < target.x)
					{
						currentWorldRect.x = target.x;
						moving = false;
						setAction(ACTION::IDLE);
					}
					break;
				case game_objects::DIRECTION::UP:
					currentWorldRect.y -= MOVE_SPEED * GetFrameTime() * screenConfig::currentScreenWidth;
					if (currentWorldRect.y < target.y)
					{
						currentWorldRect.y = target.y;
						moving = false;
						setAction(ACTION::IDLE);
					}
					break;
				case game_objects::DIRECTION::RIGHT:
					currentWorldRect.x += MOVE_SPEED * GetFrameTime() * screenConfig::currentScreenWidth;
					if (currentWorldRect.x > target.x)
					{
						currentWorldRect.x = target.x;
						moving = false;
						setAction(ACTION::IDLE);
					}
					break;
				case game_objects::DIRECTION::DOWN:
					currentWorldRect.y += MOVE_SPEED * GetFrameTime() * screenConfig::currentScreenWidth;
					if (currentWorldRect.y > target.y)
					{
						currentWorldRect.y = target.y;
						moving = false;
						setAction(ACTION::IDLE);
					}
					break;
				default:
					break;
				}
			}
		}
		void Player::setDirection(DIRECTION dir)
		{
			if (currentDirection != dir)
			{
				currentDirection = dir;
				currentSourceRec.x = 0;
				currentSourceRec.y = static_cast<float>((textureAtlas->height / TOTAL_ANIMATIONS_TYPES) * (static_cast<int>(currentDirection)* ACTION_TYPES + static_cast<int>(currentAction)));
			}
		}
		void Player::move()
		{
			switch (currentDirection)
			{
			case game_objects::DIRECTION::LEFT:
				target.x = currentWorldRect.x - currentTileSize.x;
				currentTilesPosition.x--;
				break;
			case game_objects::DIRECTION::UP:
				target.y = currentWorldRect.y - currentTileSize.y;
				currentTilesPosition.y--;
				break;
			case game_objects::DIRECTION::RIGHT:
				target.x = currentWorldRect.x + currentTileSize.x;
				currentTilesPosition.x++;
				break;
			case game_objects::DIRECTION::DOWN:
				target.y = currentWorldRect.y + currentTileSize.y;
				currentTilesPosition.y++;
				break;
			default:
				break;
			}
			moving = true;
		}
		void Player::setAction(ACTION action)
		{
			if (currentAction != action)
			{
				currentAction = action;
				currentSourceRec.x = 0;
				currentSourceRec.y = static_cast<float>((textureAtlas->height / TOTAL_ANIMATIONS_TYPES) * (static_cast<int>(currentDirection)* ACTION_TYPES + static_cast<int>(currentAction)));
			}
		}
		ACTION Player::getAction()
		{
			return currentAction;
		}
		Vector2 Player::getCurrentTilePosition()
		{
			return currentTilesPosition;
		}
		void Player::setPositionFromZero(Vector2 newTilePos)
		{
			currentTilesPosition = newTilePos;
			currentWorldRect.x += currentTileSize.x * currentTilesPosition.x;
			currentWorldRect.y += currentTileSize.y * currentTilesPosition.y;
		}
		bool Player::isMoving()
		{
			return moving;
		}
		void Player::moveHalfTile()
		{
			currentWorldRect.x += currentWorldRect.width / 2;
		}
	}
}