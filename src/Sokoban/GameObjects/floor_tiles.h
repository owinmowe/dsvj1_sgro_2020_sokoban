#ifndef FLOOR_TILES_H
#define FLOOR_TILES_H

#include "raylib.h"
#include "game_objects.h"

namespace game_objects
{

	const int TILES_SKINS_AMMOUNT = 3;
	const int TILES_TYPES_AMMOUNT = 3;
	const int GRID_SIZE_X = 25;
	const int GRID_SIZE_Y = 12;

	class FloorTiles
	{
	public:
		FloorTiles(Rectangle newRec);
		~FloorTiles();
		void draw();
		void activate();
		void moveHalfTile();
		void makeTrigger();
		void makeWall();
		void setSkin(SKIN_TYPE type);
		bool isActive();
		bool isTrigger();
		bool isWall();
	private:
		bool active = false;
		bool trigger = false;
		bool wall = false;
		bool glowing = false;
		Color activeColor = { 255, 255, 255, 0 };
		Texture2D* texture = nullptr;
		Rectangle currentSourceRec = { 0, 0, 0, 0 };
		Rectangle currentWorldRec = { 0, 0, 0, 0 };
	};
}

#endif // !FLOOR_TILES_H

