#include "ui.h"

#include "Scenes/scene_manager.h"
#include "ScreenConfig/screen_config.h"
#include "Input/input.h"
#include "EventSystem/event_system.h"
#include "Draw/textures.h"

namespace ui
{

	using namespace textures;

	const float BUTTON_BASE_WIDTH = 1 / 4.f;
	const float BUTTON_BASE_HEIGHT = 1 / 10.f;

	const int maxSizeTextButton = 25;

	void GUIcomponent::setRectangle(Rectangle rec)
	{
		collider = rec;
	}
	Rectangle GUIcomponent::getRectangle()
	{
		return collider;
	}

	const char* Button::getText()
	{
		return text.c_str();
	}
	void Button::drawComponent()
	{
		if(active)
		{
			Texture2D* buttonTexture = nullptr;
			if (locked)
			{
				buttonTexture = textures::getUiTexture(textures::UI_TEXTURES_ID::BUTTON_LOCKED);
			}
			else if(selected)
			{
				buttonTexture = textures::getUiTexture(textures::UI_TEXTURES_ID::BUTTON_SELECTED);
			}
			else
			{
				buttonTexture = textures::getUiTexture(textures::UI_TEXTURES_ID::BUTTON_NORMAL);
			}
			if(buttonTexture != nullptr)
			{
				DrawTexturePro(*buttonTexture, { 0,0, static_cast<float>(buttonTexture->width), static_cast<float>(buttonTexture->height) }, getRectangle(), { 0,0 }, 0, WHITE);

				drawTextWithSecondFont(getText(), getRectangle().x + getRectangle().width / 2,
					getRectangle().y + getRectangle().height / 2, screenConfig::textScreenModifier(TEXT_SIZE_SCALE), BLACK);
			}
		}
	}
	void Button::update()
	{
		if (active)
		{
			if (selected)
			{
				if (input::currentInput.enter)
				{
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::SELECT_MENU);
					action();
				}
			}
		}
	}

	void drawAllComponents(const int GUI_SIZE, GUIcomponent* GUI_Ptr[])
	{
		for (int i = 0; i < GUI_SIZE; i++)
		{
			if (GUI_Ptr[i] != nullptr)
			{
				GUI_Ptr[i]->drawComponent();
			}
		}
	}

	void drawTextWithSecondFont(const char* text, float posX, float posY, float fontSize, Color color)
	{
		float auxPosX = posX - MeasureTextEx(*getFont(FONTS_ID::BASE_FONT), text, fontSize, 1).x / 2;
		float auxPosY = posY - MeasureTextEx(*getFont(FONTS_ID::BASE_FONT), text, fontSize, 1).y / 2;
		DrawTextEx(*getFont(FONTS_ID::BASE_FONT), text, { auxPosX, auxPosY }, fontSize, 1, color);
	}

	void upDownGUILogic(const int GUI_SIZE, GUIcomponent* GUI_Ptr[])
	{

		for (int i = 0; i < GUI_SIZE; i++)
		{
			GUI_Ptr[i]->update();
			if (sceneConfig::sceneChange)
			{
				return;
			}
		}
		if (input::currentInput.pressed_Down)
		{
			for (int i = 0; i < GUI_SIZE; i++)
			{
				if (GUI_Ptr[i]->selected && i + 1 < GUI_SIZE)
				{
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE_MENU);
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[i + 1]->selected = true;
					break;
				}
				else if (GUI_Ptr[i]->selected && i != 0)
				{
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE_MENU);
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[0]->selected = true;
					break;
				}
			}
		}
		else if (input::currentInput.pressed_Up)
		{
			for (int i = GUI_SIZE - 1; i >= 0; i--)
			{
				if (GUI_Ptr[i]->selected && i > 0)
				{
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE_MENU);
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[i - 1]->selected = true;
					break;
				}
				else if (GUI_Ptr[i]->selected && GUI_SIZE - 1 != 0)
				{
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE_MENU);
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[GUI_SIZE - 1]->selected = true;
					break;
				}
			}
		}
	}
	void levelSelectGUILogic(const int GUI_SIZE, GUIcomponent* GUI_Ptr[])
	{

		for (int i = 0; i < GUI_SIZE; i++)
		{
			GUI_Ptr[i]->update();
			if (sceneConfig::sceneChange)
			{
				return;
			}
		}
		if (input::currentInput.pressed_Down)
		{
			for (int i = 0; i < GUI_SIZE; i++)
			{
				if (GUI_Ptr[i]->selected && i <= 6 && i != 0)
				{
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE_MENU);
					GUI_Ptr[i]->selected = false;
					if(!GUI_Ptr[i + 3]->locked)
					{
						GUI_Ptr[i + 3]->selected = true;
					}
					else
					{
						GUI_Ptr[0]->selected = true;
					}
					return;
				}
				else if (GUI_Ptr[i]->selected && i != 0)
				{
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE_MENU);
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[0]->selected = true;
					return;
				}
				else if(GUI_Ptr[0]->selected)
				{
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE_MENU);
					GUI_Ptr[0]->selected = false;
					if(!GUI_Ptr[2]->locked)
					{
						GUI_Ptr[2]->selected = true;
					}
					else
					{
						GUI_Ptr[1]->selected = true;
					}
					return;
				}
			}
		}
		else if (input::currentInput.pressed_Up)
		{
			for (int i = 0; i < GUI_SIZE; i++)
			{
				if (GUI_Ptr[i]->selected && i != 1 && i != 2 && i != 3 && i != 0)
				{
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE_MENU);
					GUI_Ptr[i]->selected = false;
					if(i - 3 > 0)
					{
						GUI_Ptr[i - 3]->selected = true;
					}
					return;
				}
				else if (GUI_Ptr[i]->selected && i != 0)
				{
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE_MENU);
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[0]->selected = true;
					return;
				}
				else if (GUI_Ptr[0]->selected)
				{
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE_MENU);
					GUI_Ptr[i]->selected = false;
					if (!GUI_Ptr[i + 8]->locked)
					{
						GUI_Ptr[i + 8]->selected = true;
					}
					else if (!GUI_Ptr[i + 5]->locked)
					{
						GUI_Ptr[i + 5]->selected = true;
					}
					else if(!GUI_Ptr[i + 2]->locked)
					{
						GUI_Ptr[i + 2]->selected = true;
					}
					else
					{
						GUI_Ptr[1]->selected = true;
					}
					return;
				}
			}
		}
		else if (input::currentInput.pressed_Right)
		{
			for (int i = 0; i < GUI_SIZE; i++)
			{
				if (GUI_Ptr[i]->selected && i != 3 && i != 6 && i != 9 && i != 0)
				{
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE_MENU);
					GUI_Ptr[i]->selected = false;
					if(!GUI_Ptr[i + 1]->locked)
					{
						GUI_Ptr[i + 1]->selected = true;
					}
					else if(!GUI_Ptr[i + 2]->locked && i != 2 && i != 5 && i != 8)
					{
						GUI_Ptr[i + 2]->selected = true;
					}
					else
					{
						GUI_Ptr[i]->selected = true;
					}
					return;
				}
				else if (GUI_Ptr[i]->selected && i != 0)
				{
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE_MENU);
					GUI_Ptr[i]->selected = false;
					if(i - 2 > 0)
					{
						GUI_Ptr[i - 2]->selected = true;
					}
					else
					{
						GUI_Ptr[1]->selected = true;
					}
					return;
				}
			}
		}
		else if (input::currentInput.pressed_Left)
		{
			for (int i = 0; i < GUI_SIZE; i++)
			{
				if (GUI_Ptr[i]->selected && i != 1 && i != 4 && i != 7 && i != 0)
				{
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE_MENU);
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[i - 1]->selected = true;
					return;
				}
				else if (GUI_Ptr[i]->selected && i != 0)
				{
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE_MENU);
					GUI_Ptr[i]->selected = false;
					if(GUI_Ptr[i + 2]->locked && GUI_Ptr[i + 1]->locked)
					{
						GUI_Ptr[i]->selected = true;
					}
					else if(GUI_Ptr[i + 2]->locked)
					{
						GUI_Ptr[i + 1]->selected = true;
					}
					else
					{
						GUI_Ptr[i + 2]->selected = true;
					}
					return;
				}
			}
		}
	}

}