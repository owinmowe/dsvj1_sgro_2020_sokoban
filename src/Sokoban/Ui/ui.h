#ifndef UI_H
#define UI_H

#include "raylib.h"
#include <string>

namespace ui
{

	class GUIcomponent
	{
	public:
		bool selected = false;
		bool active = false;
		bool locked = false;
		virtual void update() = 0;
		virtual void action() = 0;
		virtual void drawComponent() = 0;
		void setRectangle(Rectangle rec);
		Rectangle getRectangle();
	private:
		Rectangle collider = { 0,0,0,0 };
	};
	void upDownGUILogic(const int GUI_SIZE, GUIcomponent* GUI_Ptr[]);
	void levelSelectGUILogic(const int GUI_SIZE, GUIcomponent* GUI_Ptr[]);
	void drawAllComponents(const int GUI_SIZE, GUIcomponent* GUI_Ptr[]);
	const float TEXT_SIZE_SCALE = .04f;

	class Button : public GUIcomponent
	{
	public:
		void update() override;
		void drawComponent() override;
		const char* getText();
	protected:
		std::string text;
	};

	void drawTextWithSecondFont(const char* text, float posX, float posY, float fontSize, Color color);

}

#endif UI_H
