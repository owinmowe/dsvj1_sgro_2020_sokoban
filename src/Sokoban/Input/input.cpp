#include "Input.h"
#include <iostream>

namespace input
{

    Input currentInput;

    Input checkInput()
    {

        Input newInput;

        if (IsKeyPressed(KEY_ENTER)) { newInput.enter = true; }

        if (IsKeyPressed(KEY_LEFT) || IsKeyPressed(KEY_A)) { newInput.pressed_Left = true; }
        if (IsKeyPressed(KEY_S) || IsKeyPressed(KEY_DOWN)) { newInput.pressed_Down = true; }
        if (IsKeyPressed(KEY_W) || IsKeyPressed(KEY_UP)) { newInput.pressed_Up = true; }
        if (IsKeyPressed(KEY_RIGHT) || IsKeyPressed(KEY_D)) { newInput.pressed_Right = true; }

        if (IsKeyDown(KEY_LEFT) || IsKeyDown(KEY_A)) { newInput.down_Left = true; }
        if (IsKeyDown(KEY_S) || IsKeyDown(KEY_DOWN)) { newInput.down_Down = true; }
        if (IsKeyDown(KEY_W) || IsKeyDown(KEY_UP)) { newInput.down_Up = true; }
        if (IsKeyDown(KEY_RIGHT) || IsKeyDown(KEY_D)) { newInput.down_Right = true; }

#if DEBUG
        if (IsKeyPressed(KEY_P)) { newInput.pressed_P = true; }
        if (IsKeyPressed(KEY_T)) { newInput.pressed_T = true; }
        if (IsKeyPressed(KEY_F)) { newInput.pressed_F = true; }
        if (IsKeyPressed(KEY_G)) { newInput.pressed_G = true; }
        if (IsKeyPressed(KEY_H)) { newInput.pressed_H = true; }
#endif

        return newInput;
    }

    void updateInputGame()
    {
        currentInput = checkInput();
    }
}