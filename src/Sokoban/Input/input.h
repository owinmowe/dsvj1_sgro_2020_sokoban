#ifndef INPUT_H
#define INPUT_H

#include "raylib.h"

namespace input
{

	struct Input
	{

		bool enter = false;

		bool pressed_Left = false;
		bool pressed_Down = false;
		bool pressed_Up = false;
		bool pressed_Right = false;

		bool down_Left = false;
		bool down_Down = false;
		bool down_Up = false;
		bool down_Right = false;

#if DEBUG
		bool pressed_P = false;
		bool pressed_T = false;
		bool pressed_F = false;
		bool pressed_G = false;
		bool pressed_H = false;
#endif

	};

	void updateInputGame();

	extern Input currentInput;

}

#endif 