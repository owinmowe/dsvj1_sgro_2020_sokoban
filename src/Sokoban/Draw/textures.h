#ifndef TEXTURES_H
#define TEXTURES_H

#include "raylib.h"
#include <vector>

namespace textures
{
	enum class FONTS_ID { BASE_FONT };
	Font* getFont(FONTS_ID id);

	enum class UI_TEXTURES_ID { BUTTON_NORMAL, BUTTON_SELECTED, BUTTON_LOCKED, CURSOR, OVERLAY, SELECT_SCREEN_BACKGROUND, GENERAL_BACKGROUND, KEYS_OVERLAYS};
	Texture2D* getUiTexture(UI_TEXTURES_ID id);

	enum class GAMEOBJECTS_TEXTURES_ID {PLAYER, BOULDERS, TILES, SKELETON, GHOUL, MUMMY, SKELETON_ARCHER, GHOUL_ARCHER, ANUBIS, DEATH_KNIGHT, LICH, MUMMY_PHARAOH };
	Texture2D* getGameObjectTexture(GAMEOBJECTS_TEXTURES_ID id);

	enum class BACKGROUND_TEXTURES_ID {GAME_BACKGROUND_OVERLAY, GAME_BACKGROUND};
	Texture2D* getBackgroundTexture(BACKGROUND_TEXTURES_ID id);

	void LoadUiTextures();
	void UnloadUiTextures();
	void LoadGameTextures();
	void UnloadGameTextures();

}

#endif TEXTURES_H
