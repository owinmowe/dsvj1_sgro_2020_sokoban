#include "draw.h"
#include "ScreenConfig/screen_config.h"
#include "Scenes/scene_manager.h"

void frameCounter();

void drawGame()
{
    frameCounter();
    BeginDrawing();
    ClearBackground(BLACK);
    if(sceneConfig::currentScene_Ptr != nullptr)
    {
        sceneConfig::currentScene_Ptr->draw();
    }
    EndDrawing();
}

void frameCounter()
{
    screenConfig::currentFrame++;
    if (screenConfig::currentFrame == screenConfig::FRAMES_PER_SECOND)
    {
        screenConfig::currentFrame = 0;
    }
}