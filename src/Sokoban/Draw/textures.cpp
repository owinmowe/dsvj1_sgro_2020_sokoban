#include "textures.h"
#include "raylib.h"
#include <string>
#include <iostream>

namespace textures
{

	const int FONT_LOAD_SIZE = 225;
	const int MAX_SIZE_FILEPATH = 100;

	struct FontFile
	{
		Font font;
		char FILE_PATH[MAX_SIZE_FILEPATH];
	};

	struct TextureFile
	{
		Texture2D texture;
		char FILE_PATH[MAX_SIZE_FILEPATH];
	};

	std::vector<FontFile> fontFiles;
	std::vector<TextureFile> uiTextureFiles;
	std::vector<TextureFile> backgroundTextureFiles;
	std::vector<TextureFile> gameObjectsTextureFiles;

	void setFontsVector()
	{
		fontFiles.push_back({ Font(), "res/assets/textures/fonts/base_font.ttf" });
	}

	void setUiTexturesVector()
	{
		uiTextureFiles.push_back({ Texture2D(), "res/assets/textures/ui/button_normal.png" });
		uiTextureFiles.push_back({ Texture2D(), "res/assets/textures/ui/button_selected.png" });
		uiTextureFiles.push_back({ Texture2D(), "res/assets/textures/ui/button_locked.png" });
		uiTextureFiles.push_back({ Texture2D(), "res/assets/textures/ui/cursor.png" });
		uiTextureFiles.push_back({ Texture2D(), "res/assets/textures/ui/overlay.png" });
		uiTextureFiles.push_back({ Texture2D(), "res/assets/textures/ui/select_screen_overlay.png" });
		uiTextureFiles.push_back({ Texture2D(), "res/assets/textures/ui/background.png" });
		uiTextureFiles.push_back({ Texture2D(), "res/assets/textures/ui/keys_overlay.png" });
	}

	void setBackgroundTexturesVector()
	{
		backgroundTextureFiles.push_back({ Texture2D(), "res/assets/textures/backgrounds/game_background_overlay.png" });
		backgroundTextureFiles.push_back({ Texture2D(), "res/assets/textures/backgrounds/game_background.png" });

	}

	void setGameObjectsTexturesVector()
	{
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/in_game/player/player.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/in_game/boulders.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/in_game/tiles.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/in_game/level_summons/Summon-1.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/in_game/level_summons/Summon-2.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/in_game/level_summons/Summon-3.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/in_game/level_summons/Summon-4.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/in_game/level_summons/Summon-5.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/in_game/level_summons/Summon-6.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/in_game/level_summons/Summon-7.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/in_game/level_summons/Summon-8.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/in_game/level_summons/Summon-9.png" });
	}

	void LoadUiTextures()
	{
		setFontsVector();
		for (FontFile& file : fontFiles)
		{
			file.font = LoadFontEx(file.FILE_PATH, FONT_LOAD_SIZE, 0, NULL);
		}

		setUiTexturesVector();
		for (TextureFile& file : uiTextureFiles)
		{
			file.texture = LoadTexture(file.FILE_PATH);
		}
	}
	void UnloadUiTextures()
	{
		for(FontFile& file : fontFiles)
		{
			UnloadFont(file.font);
		}
		fontFiles.clear();

		for(TextureFile& file : uiTextureFiles)
		{
			UnloadTexture(file.texture);
		}
		uiTextureFiles.clear();
	}

	void LoadGameTextures()
	{
		setBackgroundTexturesVector();
		for(TextureFile& file : backgroundTextureFiles)
		{
			file.texture = LoadTexture(file.FILE_PATH);
		}

		setGameObjectsTexturesVector();
		for(TextureFile& file : gameObjectsTextureFiles)
		{
			file.texture = LoadTexture(file.FILE_PATH);
		}
	}
	void UnloadGameTextures()
	{
		for (TextureFile& file : backgroundTextureFiles)
		{
			UnloadTexture(file.texture);
		}
		backgroundTextureFiles.clear();

		for (TextureFile& file : gameObjectsTextureFiles)
		{
			UnloadTexture(file.texture);
		}
		gameObjectsTextureFiles.clear();
	}
	Font* getFont(FONTS_ID id)
	{
		return &fontFiles[static_cast<int>(id)].font;
	}
	Texture2D* getUiTexture(UI_TEXTURES_ID id)
	{
		return &uiTextureFiles[static_cast<int>(id)].texture;
	}
	Texture2D* getGameObjectTexture(GAMEOBJECTS_TEXTURES_ID id)
	{
		return &gameObjectsTextureFiles[static_cast<int>(id)].texture;
	}
	Texture2D* getBackgroundTexture(BACKGROUND_TEXTURES_ID id)
	{
		return &backgroundTextureFiles[static_cast<int>(id)].texture;
	}
}