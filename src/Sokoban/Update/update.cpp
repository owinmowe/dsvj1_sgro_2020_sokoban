#include "update.h"

#include "Scenes/scene_manager.h"

void updateGame()
{
	if(sceneConfig::currentScene_Ptr != nullptr)
	{
		sceneConfig::currentScene_Ptr->update();
	}
}