#include "selection_screen.h"
#include "Draw/textures.h"
#include "Audio/audio.h"
#include "ScreenConfig/screen_config.h"

namespace selection_screen
{

	const int MAX_LEVEL_STORAGE_POSITION = 0;

	SelectionScreen::SelectionScreen()
	{
		init();
	}
	SelectionScreen::~SelectionScreen()
	{
		deInit();
	}
	void SelectionScreen::init()
	{
		overlayTexture = textures::getUiTexture(textures::UI_TEXTURES_ID::OVERLAY);
		overlaySelectionTexture = textures::getUiTexture(textures::UI_TEXTURES_ID::SELECT_SCREEN_BACKGROUND);
		backgroundTexture = textures::getUiTexture(textures::UI_TEXTURES_ID::GENERAL_BACKGROUND);
		backgroundTexture->width = screenConfig::currentScreenWidth;
		backgroundTexture->height = screenConfig::currentScreenHeight;
		if(LoadStorageValue(MAX_LEVEL_STORAGE_POSITION) == 0)
		{
			SaveStorageValue(MAX_LEVEL_STORAGE_POSITION, sceneConfig::currentMaxLevel);
		}
		else
		{
			sceneConfig::currentMaxLevel = LoadStorageValue(MAX_LEVEL_STORAGE_POSITION);
		}
		for (int i = 0; i < GUI_SIZE; i++)
		{
			GUIcomponents[i] = nullptr;
		}
		GUIcomponents[0] = new BackButton;
		GUIcomponents[0]->setRectangle(screenConfig::screenModifier(BUTTON_BACK_REC));
		GUIcomponents[0]->active = true;
		GUIcomponents[1] = new LevelButton(1);
		GUIcomponents[1]->setRectangle(screenConfig::screenModifier(BUTTON_LEVEL_ONE_REC));
		GUIcomponents[1]->active = true;
		GUIcomponents[1]->selected = true;
		GUIcomponents[2] = new LevelButton(2);
		GUIcomponents[2]->setRectangle(screenConfig::screenModifier(BUTTON_LEVEL_TWO_REC));
		GUIcomponents[2]->active = true;
		GUIcomponents[3] = new LevelButton(3);
		GUIcomponents[3]->setRectangle(screenConfig::screenModifier(BUTTON_LEVEL_THREE_REC));
		GUIcomponents[3]->active = true;
		GUIcomponents[4] = new LevelButton(4);
		GUIcomponents[4]->setRectangle(screenConfig::screenModifier(BUTTON_LEVEL_FOUR_REC));
		GUIcomponents[4]->active = true;
		GUIcomponents[5] = new LevelButton(5);
		GUIcomponents[5]->setRectangle(screenConfig::screenModifier(BUTTON_LEVEL_FIVE_REC));
		GUIcomponents[5]->active = true;
		GUIcomponents[6] = new LevelButton(6);
		GUIcomponents[6]->setRectangle(screenConfig::screenModifier(BUTTON_LEVEL_SIX_REC));
		GUIcomponents[6]->active = true;
		GUIcomponents[7] = new LevelButton(7);
		GUIcomponents[7]->setRectangle(screenConfig::screenModifier(BUTTON_LEVEL_SEVEN_REC));
		GUIcomponents[7]->active = true;
		GUIcomponents[8] = new LevelButton(8);
		GUIcomponents[8]->setRectangle(screenConfig::screenModifier(BUTTON_LEVEL_EIGHT_REC));
		GUIcomponents[8]->active = true;
		GUIcomponents[9] = new LevelButton(9);
		GUIcomponents[9]->setRectangle(screenConfig::screenModifier(BUTTON_LEVEL_NINE_REC));
		GUIcomponents[9]->active = true;
		for (int i = 2; i < GUI_SIZE; i++)
		{
			if(i > sceneConfig::currentMaxLevel)
			{
				GUIcomponents[i]->locked = true;
			}
		}
	}
	void SelectionScreen::deInit()
	{

	}
	void SelectionScreen::update()
	{
		levelSelectGUILogic(GUI_SIZE, GUIcomponents);
		audio::menuAudioUpdate();
	}
	void SelectionScreen::draw()
	{
		DrawTexture(*backgroundTexture, 0, 0, WHITE);
		DrawTexturePro(*overlayTexture, { 0, 0, static_cast<float>(overlayTexture->width), static_cast<float>(overlayTexture->height) }, screenConfig::screenModifier(OVERLAY_REC), { 0, 0 }, 0, WHITE);
		DrawTexturePro(*overlaySelectionTexture, { 0, 0, static_cast<float>(overlaySelectionTexture->width), static_cast<float>(overlaySelectionTexture->height) }, screenConfig::screenModifier(OVERLAY_SELECTION_REC), { 0, 0 }, 0, WHITE);
		for (int i = 0; i < GUI_SIZE; i++)
		{
			if (GUIcomponents[i] != nullptr)
			{
				GUIcomponents[i]->drawComponent();
			}
		}
	}

	BackButton::BackButton()
	{
		text = "Back";
	}
	void BackButton::action()
	{
		sceneConfig::ChangeSceneTo(sceneConfig::Scene::MAIN_MENU);
	}

	LevelButton::LevelButton(int level)
	{
		currentLevel = level;
		text = "Level " + std::to_string(level);
	}
	void LevelButton::action()
	{
		sceneConfig::currentLevel = currentLevel;
		sceneConfig::ChangeSceneTo(sceneConfig::Scene::IN_GAME);
	}
}