#ifndef CREDITS_H
#define CREDITS_H

#include "scenes_base.h"
#include "scene_manager.h"
#include "Ui/ui.h"

namespace credits
{

	using namespace ui;

	const char TITLE_TEXT[] = "Credits";
	const float TITLE_SIZE = 1 / 9.f;
	const float TITLE_POSITION = 1 / 15.f;
	const float TEXT_STARTING_POSITION = 1 / 6.f;
	const float TEXT_SEPARATION = 1 / 25.f;
	const float TEXT_SIZE = 1 / 50.f;
	const char CREDITS_TEXT_1[] = "Made by Adrian Sgro with Raylib library (www.raylib.com)";
	const char CREDITS_TEXT_2[] = "Font: Evil Army (www.dafont.com)";
	const char CREDITS_TEXT_3[] = "Textures: Textures legally licenced from 'Craftpix' (www.craftpix.net)";
	const char CREDITS_TEXT_4[] = "SFX: 'Demo Ancient Magic Pack FREE' by SF Audio Guild of unity store (www.assetstore.unity.com)";
	const char CREDITS_TEXT_5[] = "SFX: 'Dark Magic' by MGWSoundDesing of unity store (www.assetstore.unity.com)";
	const char CREDITS_TEXT_6[] = "Menu Music: 'Emotional Epic Trailer' by Purple Planet Music (www.purple-planet.com)";
	const char CREDITS_TEXT_7[] = "Game Music: 'Predator' by Purple Planet Music (www.purple-planet.com)";
	const Rectangle OVERLAY_REC = { .1f, 0.2f, .8f, .65f };

	const int GUI_SIZE = 1;
	const Rectangle BUTTON_BACK_REC = { 0.3625f, 0.875f, .275f, .1f };

	class Credits : public SceneBase
	{

	private:
		void init() override;
		GUIcomponent* GUIComponent[GUI_SIZE];
		Texture2D* backgroundTexture = nullptr;
		Texture2D* overlayTexture = nullptr;

	public:
		Credits();
		~Credits();
		void deInit() override;
		void update() override;
		void draw() override;

	};

	class BackButton : public Button
	{
	public:
		BackButton();
		bool selected = false;
		void action() override;

	private:
	};

}

#endif