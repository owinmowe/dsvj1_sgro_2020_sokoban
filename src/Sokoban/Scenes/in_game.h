
#ifndef IN_GAME_H
#define IN_GAME_H

#include <vector>
#include "scenes_base.h"
#include "scene_manager.h"
#include "ScreenConfig/screen_config.h"
#include "Audio/audio.h"
#include "Draw/textures.h"
#include "Input/input.h"
#include "EventSystem/event_system.h"
#include "Ui/ui.h"
#include "GameObjects/level_summons.h"
#include "GameObjects/player.h"
#include "GameObjects/floor_tiles.h"
#include "gameObjects/boulder.h"

namespace in_game
{

	const int MAX_CURRENT_LEVEL = 9;
	const Vector2 TILE_SIZE{ .035f, .05f };
	const Vector2 EVEN_GRID_OFFSET{ 0.0625f, 0.05f };
	const int ZOMBIE_STARTING_LEVEL = 4;
	const int MUMMY_STARTING_LEVEL = 7;

	const Rectangle BUTTON_NEXT_LEVEL_REC = { 0.4f, 0.675f, .2f, .1f };
	const Rectangle OVERLAY_REC = { 0.35f, 0.05f, .3f, .595f };
	const Rectangle KEYS_OVERLAY_REC = { 0.05f, 0.65f, .3f, .3f };
	const Rectangle UNPAUSE_REC = { 0.375f, 0.1f, .25f, .1f };
	const Rectangle RESET_REC = { 0.375f, 0.2f, .25f, .1f };
	const Rectangle MUTE_MUSIC_REC = { 0.375f, 0.3f, .25f, .1f };
	const Rectangle MUTE_SFX_REC = { 0.375f, 0.4f, .25f, .1f };
	const Rectangle BUTTON_BACK_REC = { 0.375f, 0.5f, .25f, .1f };
	const int GUI_SIZE = 5;

	class BackToTheSelectScreen : public ui::Button
	{
	public:
		BackToTheSelectScreen();
	private:
		void action() override;
	};

	class InGame : public SceneBase
	{
	public:
		InGame();
		~InGame();
		void draw() override;
		void update() override;
	private:
		game_objects::player::Player* currentPlayer = nullptr;
		game_objects::summons::LevelSummon* currentSummon = nullptr;
		game_objects::FloorTiles* currentTiles[game_objects::GRID_SIZE_X][game_objects::GRID_SIZE_Y];
		std::vector<game_objects::Boulder*> boulders;
		Texture2D* backgroundTexture = nullptr;
		Texture2D* overlayTexture = nullptr;
		Texture2D* keysOverlayTexture = nullptr;
		ui::GUIcomponent* pauseMenu[GUI_SIZE];
		BackToTheSelectScreen* nextLevelButton;
		void checkLevelComplete();
		void winLevel();
		bool gameEnded = false;
		void loadLevelObjects(int level);
		void setObjectsStartingPositionsByLevel(int level);
		void centerLevel();
		void setSkinByLevel(int level);
		void tryMovePlayer(game_objects::DIRECTION dir);
		bool checkForObstacles(int playerPosX, int playerPosY, game_objects::DIRECTION dir);
		bool doubleObstacle(int i, game_objects::DIRECTION dir);
		void deInit() override;
		void init() override;
	};

	class UnpauseButton : public ui::Button
	{
	public:
		UnpauseButton();
	private:
		void action() override;
	};
	class ResetButton : public ui::Button
	{
	public:
		ResetButton();
	private:
		void action() override;
	};
	class MuteMusicButton : public ui::Button
	{
	public:
		MuteMusicButton();
	private:
		void action() override;
	};
	class MuteSFXButton : public ui::Button
	{
	public:
		MuteSFXButton();
	private:
		void action() override;
	};
	class BackButton : public ui::Button
	{
	public:
		BackButton();
	private:
		void action() override;
	};
}

#endif

