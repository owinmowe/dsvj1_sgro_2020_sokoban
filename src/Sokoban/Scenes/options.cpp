#include "options.h"
#include <sstream>
#include <string>

namespace options
{
	Options::Options()
	{
		init();
	}
	Options::~Options()
	{
		deInit();
	}
	void Options::init()
	{
		overlayTexture = textures::getUiTexture(textures::UI_TEXTURES_ID::OVERLAY);
		backgroundTexture = textures::getUiTexture(textures::UI_TEXTURES_ID::GENERAL_BACKGROUND);
		backgroundTexture->width = screenConfig::currentScreenWidth;
		backgroundTexture->height = screenConfig::currentScreenHeight;
		GUI_Ptr[0] = new MuteMusicButton;
		GUI_Ptr[1] = new MuteSFXButton;
		GUI_Ptr[2] = new FullScreenButton;
		GUI_Ptr[3] = new ResolutionsButton;
		GUI_Ptr[4] = new BackButton;
		for (int i = 0; i < GUI_SIZE; i++)
		{
			Rectangle aux = { static_cast<float>(screenConfig::currentScreenWidth / 2) - screenConfig::screenModifier({0, 0, GUI_WIDTH, 0 }).width / 2,
				screenConfig::screenModifier({0, GUI_STARTING_POSITION + i * GUI_SEPARATION, 0, 0 }).y,
				screenConfig::screenModifier({0, 0, GUI_WIDTH, 0 }).width,
				screenConfig::screenModifier({0, 0, 0, GUI_HEIGHT }).height };
			GUI_Ptr[i]->setRectangle(aux);
			GUI_Ptr[i]->active = true;
		}
		GUI_Ptr[0]->selected = true;
	}
	void Options::deInit()
	{
		for (int i = 0; i < GUI_SIZE; i++)
		{
			delete GUI_Ptr[i];
			GUI_Ptr[i] = nullptr;
		}
	}
	void Options::update()
	{
		ui::upDownGUILogic(GUI_SIZE, GUI_Ptr);
		audio::menuAudioUpdate();
	}
	void Options::draw()
	{
		DrawTexture(*backgroundTexture, 0, 0, WHITE);
		DrawTexturePro(*overlayTexture, { 0, 0, static_cast<float>(overlayTexture->width), static_cast<float>(overlayTexture->height) }, screenConfig::screenModifier(OVERLAY_REC), { 0, 0 }, 0, WHITE);
		drawTextWithSecondFont(TITLE_TEXT, screenConfig::textScreenModifier(.5f), screenConfig::textScreenModifier(TITLE_POSITION), screenConfig::textScreenModifier(TITLE_SIZE), DARKGRAY);
		drawAllComponents(GUI_SIZE, GUI_Ptr);
	}
	MuteMusicButton::MuteMusicButton()
	{
		sceneConfig::musicOn ? text = "Mute Music" : text = "Unmute Music";
	}
	void MuteMusicButton::action()
	{
		sceneConfig::musicOn = !sceneConfig::musicOn;
		sceneConfig::musicOn ? text = "Mute Music" : text = "Unmute Music";
	}
	MuteSFXButton::MuteSFXButton()
	{
		sceneConfig::sfxOn ? text = "Mute SFX" : text = "Unmute SFX";
	}
	void MuteSFXButton::action()
	{
		sceneConfig::sfxOn = !sceneConfig::sfxOn;
		sceneConfig::sfxOn ? text = "Mute SFX" : text = "Unmute SFX";
	}
	FullScreenButton::FullScreenButton()
	{
		IsWindowFullscreen() ? text = "Window mode" : text = "Fullscreen mode";
	}
	void FullScreenButton::action()
	{
		using namespace screenConfig;
		ToggleFullscreen();
		IsWindowFullscreen() ? text = "Window mode" : text = "Fullscreen mode";
		SetWindowPosition((GetMonitorWidth(0) - Resolutions[currentResolutionConfig].first) / 2, (GetMonitorHeight(0) - Resolutions[currentResolutionConfig].second) / 2);
	}
	ResolutionsButton::ResolutionsButton()
	{
		std::stringstream stringStream;
		stringStream << screenConfig::currentScreenWidth << " x " << screenConfig::currentScreenHeight;
		std::string currentResolution = stringStream.str();
		text = currentResolution;
	}
	void ResolutionsButton::action()
	{
		using namespace screenConfig;
		currentResolutionConfig + 1 < RESOLUTIONS_AMMOUNT ? currentResolutionConfig++ : currentResolutionConfig = 0;
		currentScreenWidth = Resolutions[currentResolutionConfig].first;
		currentScreenHeight = Resolutions[currentResolutionConfig].second;
		SetWindowSize(Resolutions[currentResolutionConfig].first, Resolutions[currentResolutionConfig].second);
		SetWindowPosition((GetMonitorWidth(0) - Resolutions[currentResolutionConfig].first) / 2, (GetMonitorHeight(0) - Resolutions[currentResolutionConfig].second) / 2);
		sceneConfig::ChangeSceneTo(sceneConfig::Scene::OPTIONS);
	}
	BackButton::BackButton()
	{
		text = "Back";
	}
	void BackButton::action()
	{
		sceneConfig::ChangeSceneTo(sceneConfig::Scene::MAIN_MENU);
	}

}