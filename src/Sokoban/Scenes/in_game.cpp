#include "in_game.h"
#include "Loader/loader.h"

namespace in_game
{

	InGame::InGame()
	{
		init();
	}
	InGame::~InGame() 
	{
		deInit();
	}
	void InGame::init()
	{
		using namespace game_objects::summons;
		audio::gameAudioStart();
		textures::LoadGameTextures();
		overlayTexture = textures::getUiTexture(textures::UI_TEXTURES_ID::OVERLAY);
		backgroundTexture = textures::getUiTexture(textures::UI_TEXTURES_ID::GENERAL_BACKGROUND);
		keysOverlayTexture = textures::getUiTexture(textures::UI_TEXTURES_ID::KEYS_OVERLAYS);
		backgroundTexture->width = screenConfig::currentScreenWidth;
		backgroundTexture->height = screenConfig::currentScreenHeight;
		pauseMenu[0] = new UnpauseButton;
		pauseMenu[0]->setRectangle(screenConfig::screenModifier(UNPAUSE_REC));
		pauseMenu[0]->active = true; 
		pauseMenu[1] = new ResetButton;
		pauseMenu[1]->setRectangle(screenConfig::screenModifier(RESET_REC));
		pauseMenu[1]->active = true;
		pauseMenu[2] = new MuteMusicButton;
		pauseMenu[2]->setRectangle(screenConfig::screenModifier(MUTE_MUSIC_REC));
		pauseMenu[2]->active = true;
		pauseMenu[3] = new MuteSFXButton;
		pauseMenu[3]->setRectangle(screenConfig::screenModifier(MUTE_SFX_REC));
		pauseMenu[3]->active = true;
		pauseMenu[4] = new BackButton;
		pauseMenu[4]->setRectangle(screenConfig::screenModifier(BUTTON_BACK_REC));
		pauseMenu[4]->active = true;
		nextLevelButton = new BackToTheSelectScreen;
		nextLevelButton->setRectangle(screenConfig::screenModifier(BUTTON_NEXT_LEVEL_REC));
		loadLevelObjects(sceneConfig::currentLevel);
		gameEnded = false;
	}
	void InGame::deInit()
	{
		textures::UnloadGameTextures();
		for (int i = 0; i < GUI_SIZE; i++)
		{
			delete pauseMenu[i];
			pauseMenu[i] = nullptr;
		}
		delete nextLevelButton;
		nextLevelButton = nullptr;
		delete currentSummon;
		currentSummon = nullptr;
		delete currentPlayer;
		currentPlayer = nullptr;
		for (int i = 0; i < game_objects::GRID_SIZE_X; i++)
		{
			for (int j = 0; j < game_objects::GRID_SIZE_Y; j++)
			{
				delete currentTiles[i][j];
			}
		}
		for (int i = 0; i < static_cast<int>(boulders.size()); i++)
		{
			delete boulders[i];
		}
	}
	void InGame::update()
	{
		if (input::currentInput.enter && !sceneConfig::pause && !nextLevelButton->active)
		{
			sceneConfig::pause = true;
			for (int i = 0; i < GUI_SIZE; i++)
			{
				pauseMenu[i]->selected = false;
			}
			pauseMenu[0]->selected = true;
		}
		else if(sceneConfig::pause && !nextLevelButton->active)
		{
			ui::upDownGUILogic(GUI_SIZE, pauseMenu);
			if (sceneConfig::resetLevel)
			{
				loadLevelObjects(sceneConfig::currentLevel);
				sceneConfig::resetLevel = false;
				sceneConfig::pause = false;
			}
		}
		else
		{
			if (nextLevelButton->active)
			{
				currentSummon->update();
				nextLevelButton->update();
			}
			else if (input::currentInput.down_Up)
			{
				tryMovePlayer(game_objects::DIRECTION::UP);
			}
			else if (input::currentInput.down_Down)
			{
				tryMovePlayer(game_objects::DIRECTION::DOWN);
			}
			else if (input::currentInput.down_Left)
			{
				tryMovePlayer(game_objects::DIRECTION::LEFT);
			}
			else if (input::currentInput.down_Right)
			{
				tryMovePlayer(game_objects::DIRECTION::RIGHT);
			}

#if DEBUG
			else if (input::currentInput.pressed_P)
			{
				winLevel();
			}
			else if(input::currentInput.pressed_F)
			{

			}
			else if (input::currentInput.pressed_G)
			{

			}
			else if (input::currentInput.pressed_H)
			{

			}
			else if (input::currentInput.pressed_T)
			{

			}
#endif
			for (int i = 0; i < static_cast<int>(boulders.size()); i++)
			{
				if (boulders[i] != nullptr)
				{
					boulders[i]->update();
				}
			}
			if (!sceneConfig::sceneChange && currentPlayer != nullptr)
			{
				currentPlayer->update();
			}
		}
		audio::gameAudioUpdate();
	}
	void InGame::winLevel()
	{
		nextLevelButton->active = true;
		nextLevelButton->selected = true;
		event_system::externalEventSystem->setEvent(event_system::EVENT_ID::WON_LEVEL);
		if (sceneConfig::currentMaxLevel == sceneConfig::currentLevel && sceneConfig::currentMaxLevel < MAX_CURRENT_LEVEL)
		{
			sceneConfig::currentMaxLevel++;
			SaveStorageValue(0, sceneConfig::currentMaxLevel);
		}
	}
	void InGame::draw()
	{
		DrawTexture(*backgroundTexture, 0, 0, WHITE);
		DrawTexturePro(*keysOverlayTexture, { 0,0, static_cast<float>(keysOverlayTexture->width), static_cast<float>(keysOverlayTexture->height) }, screenConfig::screenModifier(KEYS_OVERLAY_REC), { 0,0 }, 0, WHITE);
		for (int i = 0; i < game_objects::GRID_SIZE_X; i++)
		{
			for (int j = 0; j < game_objects::GRID_SIZE_Y; j++)
			{
				if(currentTiles[i][j]->isActive() || currentTiles[i][j]->isWall())
				{
					currentTiles[i][j]->draw();
				}
			}
		}
		currentSummon->draw();
		currentPlayer->draw();
		for (int i = 0; i < static_cast<int>(boulders.size()); i++)
		{
			if(boulders[i] != nullptr)
			{
				boulders[i]->draw();
			}
		}
		if(nextLevelButton->active)
		{
			nextLevelButton->drawComponent();
		}
		else if(sceneConfig::pause)
		{
			DrawTexturePro(*overlayTexture, { 0, 0, static_cast<float>(overlayTexture->width), static_cast<float>(overlayTexture->height) }, screenConfig::screenModifier(OVERLAY_REC), { 0, 0 }, 0, WHITE);
			for (int i = 0; i < GUI_SIZE; i++)
			{
				pauseMenu[i]->drawComponent();
			}
		}
#if DEBUG
		Vector2 auxPos = { 0.7f, 0.725f };
		Vector2 auxPos2 = { 0.7f, 0.75f };
		ui::drawTextWithSecondFont("DEBUG COMMANDS:", screenConfig::screenModifier(auxPos).x, screenConfig::screenModifier(auxPos).y, screenConfig::textScreenModifier(.02f), GRAY);
		ui::drawTextWithSecondFont("P Key: Win Level", screenConfig::screenModifier(auxPos2).x, screenConfig::screenModifier(auxPos2).y, screenConfig::textScreenModifier(.02f), GRAY);
#endif // DEBUG

	}
	void InGame::tryMovePlayer(game_objects::DIRECTION dir)
	{
		if (currentPlayer->isMoving()) { return; }
		int playerPosX = static_cast<int>(currentPlayer->getCurrentTilePosition().x);
		int playerPosY = static_cast<int>(currentPlayer->getCurrentTilePosition().y);
		currentPlayer->setDirection(dir);
		switch (dir)
		{
		case game_objects::DIRECTION::LEFT:
			if (playerPosX - 1 < 0 || !currentTiles[playerPosX - 1][playerPosY]->isActive()) { return; }
			else
			{
				currentPlayer->setAction(game_objects::player::ACTION::WALKING);
				if(checkForObstacles(playerPosX, playerPosY, game_objects::DIRECTION::LEFT))
				{
					currentPlayer->move();
				}
			}
			break;
		case game_objects::DIRECTION::UP:
			if (playerPosY - 1 < 0 || !currentTiles[playerPosX][playerPosY - 1]->isActive()) { return; }
			else
			{
				currentPlayer->setAction(game_objects::player::ACTION::WALKING);
				if(checkForObstacles(playerPosX, playerPosY, game_objects::DIRECTION::UP))
				{
					currentPlayer->move();
				}
			}
			break;
		case game_objects::DIRECTION::RIGHT:
			if (playerPosX + 1 >= game_objects::GRID_SIZE_X || !currentTiles[playerPosX + 1][playerPosY]->isActive()) { return; }
			else
			{
				currentPlayer->setAction(game_objects::player::ACTION::WALKING);
				if (checkForObstacles(playerPosX, playerPosY, game_objects::DIRECTION::RIGHT)) 
				{
					currentPlayer->move();
				}
			}
			break;
		case game_objects::DIRECTION::DOWN:
			if (playerPosY + 1 >= game_objects::GRID_SIZE_Y || !currentTiles[playerPosX][playerPosY + 1]->isActive()) { return; }
			else
			{
				currentPlayer->setAction(game_objects::player::ACTION::WALKING);
				if(checkForObstacles(playerPosX, playerPosY, game_objects::DIRECTION::DOWN))
				{
					currentPlayer->move();
				}
			}
			break;
		default:
			break;
		}
	}
	bool InGame::checkForObstacles(int playerPosX, int playerPosY, game_objects::DIRECTION dir)
	{
		switch (dir)
		{
		case game_objects::DIRECTION::LEFT:
			for (int i = 0; i < static_cast<int>(boulders.size()); i++)
			{
				if (boulders[i]->getTilePosition().x == playerPosX - 1 && boulders[i]->getTilePosition().y == playerPosY)
				{
					if(!doubleObstacle(i, dir) && currentTiles[static_cast<int>(boulders[i]->getTilePosition().x) - 1][static_cast<int>(boulders[i]->getTilePosition().y)]->isActive())
					{
						boulders[i]->move(dir);
						event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE_BOULDER);
						if (currentTiles[static_cast<int>(boulders[i]->getTilePosition().x) - 1][static_cast<int>(boulders[i]->getTilePosition().y)]->isTrigger())
						{
							if(!boulders[i]->isActive())
							{
								boulders[i]->setActive(true);
								event_system::externalEventSystem->setEvent(event_system::EVENT_ID::ACTIVATE_SWITCH);
								checkLevelComplete();
							}
						}
						else if(boulders[i]->isActive())
						{
							boulders[i]->setActive(false);
						}
						currentPlayer->setAction(game_objects::player::ACTION::PUSHING);
						return true;
					}
					else
					{
						currentPlayer->setAction(game_objects::player::ACTION::IDLE);
						return false;
					}
				}
			}
			break;
		case game_objects::DIRECTION::UP:
			for (int i = 0; i < static_cast<int>(boulders.size()); i++)
			{
				if (boulders[i]->getTilePosition().x == playerPosX && boulders[i]->getTilePosition().y == playerPosY - 1)
				{
					if (!doubleObstacle(i, dir) && currentTiles[static_cast<int>(boulders[i]->getTilePosition().x)][static_cast<int>(boulders[i]->getTilePosition().y) - 1]->isActive())
					{
						boulders[i]->move(dir);
						event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE_BOULDER);
						if(currentTiles[static_cast<int>(boulders[i]->getTilePosition().x)][static_cast<int>(boulders[i]->getTilePosition().y) - 1]->isTrigger())
						{
							boulders[i]->setActive(true);
							event_system::externalEventSystem->setEvent(event_system::EVENT_ID::ACTIVATE_SWITCH);
							checkLevelComplete();
						}
						else
						{
							boulders[i]->setActive(false);
						}
						currentPlayer->setAction(game_objects::player::ACTION::PUSHING);
						return true;
					}
					else
					{
						currentPlayer->setAction(game_objects::player::ACTION::IDLE);
						return false;
					}
				}
			}
			break;
		case game_objects::DIRECTION::RIGHT:
			for (int i = 0; i < static_cast<int>(boulders.size()); i++)
			{
				if (boulders[i]->getTilePosition().x == playerPosX + 1 && boulders[i]->getTilePosition().y == playerPosY)
				{
					if (!doubleObstacle(i, dir) && currentTiles[static_cast<int>(boulders[i]->getTilePosition().x) + 1][static_cast<int>(boulders[i]->getTilePosition().y)]->isActive())
					{
						boulders[i]->move(dir);
						event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE_BOULDER);
						if (currentTiles[static_cast<int>(boulders[i]->getTilePosition().x) + 1][static_cast<int>(boulders[i]->getTilePosition().y)]->isTrigger())
						{
							boulders[i]->setActive(true);
							event_system::externalEventSystem->setEvent(event_system::EVENT_ID::ACTIVATE_SWITCH);
							checkLevelComplete();
						}
						else
						{
							boulders[i]->setActive(false);
						}
						currentPlayer->setAction(game_objects::player::ACTION::PUSHING);
						return true;
					}
					else
					{
						currentPlayer->setAction(game_objects::player::ACTION::IDLE);
						return false;
					}
				}
			}
			break;
		case game_objects::DIRECTION::DOWN:
			for (int i = 0; i < static_cast<int>(boulders.size()); i++)
			{
				if (boulders[i]->getTilePosition().x == playerPosX && boulders[i]->getTilePosition().y == playerPosY + 1)
				{
					if (!doubleObstacle(i, dir) && currentTiles[static_cast<int>(boulders[i]->getTilePosition().x)][static_cast<int>(boulders[i]->getTilePosition().y) + 1]->isActive())
					{
						boulders[i]->move(dir);
						event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE_BOULDER);
						if (currentTiles[static_cast<int>(boulders[i]->getTilePosition().x)][static_cast<int>(boulders[i]->getTilePosition().y) + 1]->isTrigger())
						{
							boulders[i]->setActive(true);
							event_system::externalEventSystem->setEvent(event_system::EVENT_ID::ACTIVATE_SWITCH);
							checkLevelComplete();
						}
						else
						{
							boulders[i]->setActive(false);
						}
						currentPlayer->setAction(game_objects::player::ACTION::PUSHING);
						return true;
					}
					else
					{
						currentPlayer->setAction(game_objects::player::ACTION::IDLE);
						return false;
					}
				}
			}
			break;
		default:
			break;
		}
		return true;
	}
	bool InGame::doubleObstacle(int i, game_objects::DIRECTION dir)
	{
		switch (dir)
		{
		case game_objects::DIRECTION::LEFT:
			for (int j = 0; j < static_cast<int>(boulders.size()); j++)
			{
				if (static_cast<int>(boulders[i]->getTilePosition().x) - 1 == static_cast<int>(boulders[j]->getTilePosition().x) && static_cast<int>(boulders[i]->getTilePosition().y) == static_cast<int>(boulders[j]->getTilePosition().y))
				{
					return true;
				}
			}
			break;
		case game_objects::DIRECTION::UP:
			for (int j = 0; j < static_cast<int>(boulders.size()); j++)
			{
				if (static_cast<int>(boulders[i]->getTilePosition().x) == static_cast<int>(boulders[j]->getTilePosition().x) && static_cast<int>(boulders[i]->getTilePosition().y) - 1 == static_cast<int>(boulders[j]->getTilePosition().y))
				{
					return true;
				}
			}
			break;
		case game_objects::DIRECTION::RIGHT:
			for (int j = 0; j < static_cast<int>(boulders.size()); j++)
			{
				if (static_cast<int>(boulders[i]->getTilePosition().x) + 1 == static_cast<int>(boulders[j]->getTilePosition().x) && static_cast<int>(boulders[i]->getTilePosition().y) == static_cast<int>(boulders[j]->getTilePosition().y))
				{
					return true;
				}
			}
			break;
		case game_objects::DIRECTION::DOWN:
			for (int j = 0; j < static_cast<int>(boulders.size()); j++)
			{
				if (static_cast<int>(boulders[i]->getTilePosition().x) == static_cast<int>(boulders[j]->getTilePosition().x) && static_cast<int>(boulders[i]->getTilePosition().y) + 1 == static_cast<int>(boulders[j]->getTilePosition().y))
				{
					return true;
				}
			}
			break;
		default:
			break;
		}
		return false;
	}
	void InGame::loadLevelObjects(int level)
	{
		using namespace game_objects::summons;
		currentSummon = new LevelSummon(static_cast<SUMMON_TYPE>(level));
		setObjectsStartingPositionsByLevel(level);
		centerLevel();
		setSkinByLevel(level);
		nextLevelButton->active = false;
		nextLevelButton->selected = false;
	}
	void InGame::setObjectsStartingPositionsByLevel(int level)
	{
		Vector2 currentTileSize = screenConfig::screenModifier(TILE_SIZE);
		Vector2 currentGridOffset = screenConfig::screenModifier(EVEN_GRID_OFFSET);
		for (int i = 0; i < game_objects::GRID_SIZE_X; i++)
		{
			for (int j = 0; j < game_objects::GRID_SIZE_Y; j++)
			{
				currentTiles[i][j] = new game_objects::FloorTiles({ currentGridOffset.x + currentTileSize.x * i, currentGridOffset.y + currentTileSize.y * j, currentTileSize.x, currentTileSize.y });
			}
		}
		Vector2 playercurrentTilesPos{ 0, 0 };
		Vector2 playerCurrentPos = { currentGridOffset.x + currentTileSize.x * playercurrentTilesPos.x, currentGridOffset.y + currentTileSize.y * playercurrentTilesPos.y };
		currentPlayer = new game_objects::player::Player(currentTileSize, playercurrentTilesPos, playerCurrentPos);
		loader::loadLevelFromFile(level, currentTiles, currentPlayer, boulders);
		for (int i = 0; i < static_cast<int>(boulders.size()); i++)
		{
			Vector2 boulderCurrentPos = { currentGridOffset.x + currentTileSize.x * boulders[i]->getTilePosition().x, currentGridOffset.y + currentTileSize.y * boulders[i]->getTilePosition().y };
			boulders[i]->setPositionFromZero(currentTileSize, boulderCurrentPos);
		}
	}
	void InGame::setSkinByLevel(int level)
	{
		if (level < ZOMBIE_STARTING_LEVEL)
		{
			for (int i = 0; i < game_objects::GRID_SIZE_X; i++)
			{
				for (int j = 0; j < game_objects::GRID_SIZE_Y; j++)
				{
					currentTiles[i][j]->setSkin(game_objects::SKIN_TYPE::SKELETON);
				}
			}
			for (int i = 0; i < static_cast<int>(boulders.size()); i++)
			{
				boulders[i]->setSkin(game_objects::SKIN_TYPE::SKELETON);
			}
		}
		else if (level < MUMMY_STARTING_LEVEL)
		{
			for (int i = 0; i < game_objects::GRID_SIZE_X; i++)
			{
				for (int j = 0; j < game_objects::GRID_SIZE_Y; j++)
				{
					currentTiles[i][j]->setSkin(game_objects::SKIN_TYPE::ZOMBIE);
				}
			}
			for (int i = 0; i < static_cast<int>(boulders.size()); i++)
			{
				boulders[i]->setSkin(game_objects::SKIN_TYPE::ZOMBIE);
			}
		}
		else
		{
			for (int i = 0; i < game_objects::GRID_SIZE_X; i++)
			{
				for (int j = 0; j < game_objects::GRID_SIZE_Y; j++)
				{
					currentTiles[i][j]->setSkin(game_objects::SKIN_TYPE::MUMMY);
				}
			}
			for (int i = 0; i < static_cast<int>(boulders.size()); i++)
			{
				boulders[i]->setSkin(game_objects::SKIN_TYPE::MUMMY);
			}
		}
	}
	void InGame::centerLevel()
	{
		int aux = 0;
		for (int i = 0; i < game_objects::GRID_SIZE_Y; i++)
		{
			int aux2 = 0;
			for (int j = 0; j < game_objects::GRID_SIZE_X; j++)
			{
				if (currentTiles[j][i]->isActive() || currentTiles[j][i]->isWall())
				{
					aux2++;
				}
			}
			if (aux2 > aux)
			{
				aux = aux2;
			}
		}
		if (aux % 2 == 0)
		{
			for (int i = 0; i < static_cast<int>(boulders.size()); i++)
			{
				boulders[i]->moveHalfTile();
			}
			for (int i = 0; i < game_objects::GRID_SIZE_X; i++)
			{
				for (int j = 0; j < game_objects::GRID_SIZE_Y; j++)
				{
					currentTiles[i][j]->moveHalfTile();
				}
			}
			currentPlayer->moveHalfTile();
		}
	}
	void InGame::checkLevelComplete()
	{
		for (int i = 0; i < static_cast<int>(boulders.size()); i++)
		{
			if (!boulders[i]->isActive())
			{
				return;
			}
		}
		winLevel();
	}

	UnpauseButton::UnpauseButton()
	{
		text = "Unpause";
	}
	void UnpauseButton::action()
	{
		sceneConfig::pause = false;
	}
	ResetButton::ResetButton()
	{
		text = "Reset";
	}
	void ResetButton::action()
	{
		sceneConfig::resetLevel = true;
	}
	MuteMusicButton::MuteMusicButton()
	{
		sceneConfig::musicOn ? text = "Mute Music" : text = "Unmute Music";
	}
	void MuteMusicButton::action()
	{
		sceneConfig::musicOn = !sceneConfig::musicOn;
		sceneConfig::musicOn ? text = "Mute Music" : text = "Unmute Music";
	}
	MuteSFXButton::MuteSFXButton()
	{
		sceneConfig::sfxOn ? text = "Mute SFX" : text = "Unmute SFX";
	}
	void MuteSFXButton::action()
	{
		sceneConfig::sfxOn = !sceneConfig::sfxOn;
		sceneConfig::sfxOn ? text = "Mute SFX" : text = "Unmute SFX";
	}
	BackToTheSelectScreen::BackToTheSelectScreen()
	{
		text = "Back";
	}
	void BackToTheSelectScreen::action()
	{
		sceneConfig::ChangeSceneTo(sceneConfig::Scene::SELECTION_SCREEN);
	}
	BackButton::BackButton()
	{
		text = "Main Menu";
	}
	void BackButton::action()
	{
		sceneConfig::pause = false;
		sceneConfig::ChangeSceneTo(sceneConfig::Scene::MAIN_MENU);
	}
}