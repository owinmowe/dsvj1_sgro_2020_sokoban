#include "scene_manager.h"

namespace sceneConfig
{

	bool playing = true;
	bool pause = false;
	bool sfxOn = true;
	bool musicOn = true;
	bool sceneChange = false;
	int currentLevel = 1;
	int currentMaxLevel = 1;
	bool resetLevel = false;
	SceneBase* currentScene_Ptr;

	void ChangeSceneTo(sceneConfig::Scene nextScene)
	{
		delete currentScene_Ptr;
		currentScene_Ptr = nullptr;
		sceneChange = true;

		switch (nextScene)
		{
		case sceneConfig::Scene::MAIN_MENU:
			currentScene_Ptr = new main_menu::MainMenu;
			break;
		case sceneConfig::Scene::OPTIONS:
			currentScene_Ptr = new options::Options;
			break;
		case sceneConfig::Scene::CREDITS:
			currentScene_Ptr = new credits::Credits;
			break;
		case sceneConfig::Scene::IN_GAME:
			currentScene_Ptr = new in_game::InGame;
			break;
		case sceneConfig::Scene::SELECTION_SCREEN:
			currentScene_Ptr = new selection_screen::SelectionScreen;
			break;
		default:
			break;
		}

	}
}