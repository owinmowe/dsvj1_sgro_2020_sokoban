#include "main_menu.h"

namespace main_menu
{

	MainMenu::MainMenu()
	{
		init();
	}
	MainMenu::~MainMenu() 
	{
		deInit();
	}
	void MainMenu::init()
	{
		audio::menuAudioStart();
		overlayTexture = textures::getUiTexture(textures::UI_TEXTURES_ID::OVERLAY);
		backgroundTexture = textures::getUiTexture(textures::UI_TEXTURES_ID::GENERAL_BACKGROUND);
		backgroundTexture->width = screenConfig::currentScreenWidth;
		backgroundTexture->height = screenConfig::currentScreenHeight;
		GUI_Ptr[0] = new PlayButton;
		GUI_Ptr[1] = new OptionsButton;
		GUI_Ptr[2] = new CreditsButton;
		GUI_Ptr[3] = new ExitButton;
		for (int i = 0; i < GUI_SIZE; i++)
		{
			Rectangle aux = { static_cast<float>(screenConfig::currentScreenWidth / 2) - screenConfig::screenModifier({0, 0, GUI_WIDTH, 0 }).width / 2,
				screenConfig::screenModifier({0, GUI_STARTING_POSITION + i * GUI_SEPARATION, 0, 0 }).y,
				screenConfig::screenModifier({0, 0, GUI_WIDTH, 0 }).width,
				screenConfig::screenModifier({0, 0, 0, GUI_HEIGHT }).height };
				GUI_Ptr[i]->setRectangle(aux);
				GUI_Ptr[i]->active = true;
		}
		GUI_Ptr[0]->selected = true;
	}
	void MainMenu::deInit()
	{
		for (int i = 0; i < GUI_SIZE; i++)
		{
			delete GUI_Ptr[i];
			GUI_Ptr[i] = nullptr;
		}
	}
	void MainMenu::update()
	{
		ui::upDownGUILogic(GUI_SIZE, GUI_Ptr);
		audio::menuAudioUpdate();
	}
	void MainMenu::draw()
	{	
		DrawTexture(*backgroundTexture, 0, 0, WHITE);
		DrawTexturePro(*overlayTexture, { 0, 0, static_cast<float>(overlayTexture->width), static_cast<float>(overlayTexture->height) }, screenConfig::screenModifier(OVERLAY_REC), { 0, 0 }, 0, WHITE);
		drawTextWithSecondFont(TITLE_TEXT, screenConfig::textScreenModifier(.5f), screenConfig::textScreenModifier(TITLE_POSITION), screenConfig::textScreenModifier(TITLE_SIZE), DARKGRAY);
		drawTextWithSecondFont(VERSION_TEXT, screenConfig::screenModifier(VERSION_POSITION).x, screenConfig::screenModifier(VERSION_POSITION).y, screenConfig::textScreenModifier(VERSION_SIZE), DARKGRAY);
		drawAllComponents(GUI_SIZE, GUI_Ptr);
	}

	// BUTTONS LOGIC
	PlayButton::PlayButton()
	{
		text = "Play";
	}
	void PlayButton::action()
	{
		sceneConfig::ChangeSceneTo(sceneConfig::Scene::SELECTION_SCREEN);
	}
	OptionsButton::OptionsButton()
	{
		text = "Options";
	}
	void OptionsButton::action()
	{
		sceneConfig::ChangeSceneTo(sceneConfig::Scene::OPTIONS);
	}
	CreditsButton::CreditsButton()
	{
		text = "Credits";
	}
	void CreditsButton::action()
	{
		sceneConfig::ChangeSceneTo(sceneConfig::Scene::CREDITS);
	}
	ExitButton::ExitButton()
	{
		text = "Exit";
	}
	void ExitButton::action()
	{
		sceneConfig::playing = false;
	}
}