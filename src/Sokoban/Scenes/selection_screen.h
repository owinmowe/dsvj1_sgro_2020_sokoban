#ifndef SELECTION_SCREEN_H
#define SELECTION_SCREEN_H

#include "scenes_base.h"
#include "scene_manager.h"
#include "Ui/ui.h"

namespace selection_screen
{
	using namespace ui;
	const int GUI_SIZE = 10;
	const Rectangle OVERLAY_REC = { .15f, 0.025f, .7f, .85f };
	const Rectangle OVERLAY_SELECTION_REC = { .195f, 0.0825f, .6075f, .735f };
	const Rectangle BUTTON_LEVEL_ONE_REC = { .225f, .125f, .15f, .15f };
	const Rectangle BUTTON_LEVEL_TWO_REC = { .425f, .125f, .15f, .15f };
	const Rectangle BUTTON_LEVEL_THREE_REC = { .625f, .125f, .15f, .15f };
	const Rectangle BUTTON_LEVEL_FOUR_REC = { .225f, .375f, .15f, .15f };
	const Rectangle BUTTON_LEVEL_FIVE_REC = { .425f, .375f, .15f, .15f };
	const Rectangle BUTTON_LEVEL_SIX_REC = { .625f, .375f, .15f, .15f };
	const Rectangle BUTTON_LEVEL_SEVEN_REC = { .225f, .625f, .15f, .15f };
	const Rectangle BUTTON_LEVEL_EIGHT_REC = { .425f, .625f, .15f, .15f };
	const Rectangle BUTTON_LEVEL_NINE_REC = { .625f, .625f, .15f, .15f };
	const Rectangle BUTTON_BACK_REC = { 0.3625f, 0.875f, .275f, .1f };

	class SelectionScreen : public SceneBase
	{
	public:
		SelectionScreen();
		~SelectionScreen();
	private:
		void init() override;
		void deInit() override;
		void update() override;
		void draw() override;
		GUIcomponent* GUIcomponents[GUI_SIZE];
		Texture2D* backgroundTexture = nullptr;
		Texture2D* overlayTexture = nullptr;
		Texture2D* overlaySelectionTexture = nullptr;
	};

	class BackButton : public Button
	{
	public:
		BackButton();
		bool selected = false;
		void action() override;

	private:
	};

	class LevelButton : public Button
	{
	public:
		LevelButton(int level);
		bool selected = false;
		void action() override;

	private:
		int currentLevel = 1;
	};
}

#endif // !SELECTION_SCREEN_H

