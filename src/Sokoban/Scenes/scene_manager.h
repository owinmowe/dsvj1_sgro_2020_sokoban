#ifndef SCENES_MANAGER_H
#define SCENES_MANAGER_H
#include "scenes_base.h"
#include "main_menu.h"
#include "credits.h"
#include "options.h"
#include "in_game.h"
#include "selection_screen.h"

namespace sceneConfig
{

	enum class Scene { MAIN_MENU, OPTIONS, CREDITS, IN_GAME, SELECTION_SCREEN };

	extern SceneBase* currentScene_Ptr;
	extern int currentLevel;
	extern int currentMaxLevel;
	extern bool resetLevel;
	extern bool playing;
	extern bool sceneChange;
	extern bool pause;
	extern bool sfxOn;
	extern bool musicOn;

	void ChangeSceneTo(sceneConfig::Scene nextScene);

}

#endif SCENES_MANAGER_H
