#ifndef OPTIONS_H
#define OPTIONS_H

#include "scenes_base.h"
#include "scene_manager.h"
#include "Audio/audio.h"

namespace options
{

	using namespace ui;

	const char TITLE_TEXT[] = "Options";
	const float TITLE_SIZE = 1 / 9.f;
	const float TITLE_POSITION = 1 / 10.f;
	const Rectangle OVERLAY_REC = { .35f, 0.2875f, .3f, .7f };

	const int GUI_SIZE = 5;
	const float GUI_STARTING_POSITION = 1 / 3.f;
	const float GUI_SEPARATION = 1 / 8.f;
	const float GUI_WIDTH = 1 / 4.f;
	const float GUI_HEIGHT = 1 / 10.f;

	class Options : public SceneBase
	{

	private:
		void init() override;
		GUIcomponent* GUI_Ptr[GUI_SIZE];
		Texture2D* backgroundTexture = nullptr;
		Texture2D* overlayTexture = nullptr;

	public:
		Options();
		~Options();
		void deInit() override;
		void update() override;
		void draw() override;

	};

	class MuteMusicButton : public ui::Button
	{
	public:
		MuteMusicButton();
	private:
		void action() override;
	};
	class MuteSFXButton : public ui::Button
	{
	public:
		MuteSFXButton();
	private:
		void action() override;
	};
	class ResolutionsButton : public Button
	{
	public:
		ResolutionsButton();
		void action() override;
	};

	class FullScreenButton : public Button
	{
	public:
		FullScreenButton();
		void action() override;
	};

	class BackButton : public Button
	{
	public:
		BackButton();
		void action() override;
	};
}

#endif
